using UnityEngine;

namespace Game.Data {
    [CreateAssetMenu]
    public class EvaluationDatabase : ScriptableObject {
        [SerializeField] private float wonderfulRange;
        [SerializeField] private float greatRange;
        [SerializeField] private float goodRange;
        [SerializeField] private float missRange;
        public float WonderfulRange => wonderfulRange;
        public float GreatRange => greatRange;
        public float GoodRange => goodRange;
        public float MissRange => missRange;
        public float AutoRange => Time.deltaTime * 2f;
    }
}
