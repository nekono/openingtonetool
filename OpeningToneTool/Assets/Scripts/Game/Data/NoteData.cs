using System;
using Game.Note;
using UnityEngine;

namespace Game.Data {
    [Serializable]
    public struct NoteData {
        [SerializeField] private NoteType noteType;
        [SerializeField] private Note.Note note;
        public NoteType NoteType => noteType;
        public Note.Note Note => note;
    }
}