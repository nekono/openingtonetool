using System;
using UnityEngine;

namespace Game.Data {
    [CreateAssetMenu]
    public class PositionDatabase : ScriptableObject {
        [SerializeField] private PositionData lane0;
        [SerializeField] private PositionData lane1;
        [SerializeField] private PositionData lane2;
        [SerializeField] private PositionData lane3;
        [SerializeField] private PositionData lane4;
        [SerializeField] private PositionData lane5;

        public PositionData[] Data => new[] {lane0, lane1, lane2, lane3, lane4, lane5};
    }

    [Serializable]
    public struct PositionData {
        [SerializeField] private Vector2 startPosition;
        [SerializeField] private Vector2 endPosition;
        public Vector2 StartPosition => startPosition;
        public Vector2 EndPosition => endPosition;
    }
}
