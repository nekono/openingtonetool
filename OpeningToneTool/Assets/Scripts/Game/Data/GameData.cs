using System.Collections.Generic;
using Game.System;
using UnityEngine;

namespace Game.Data {
    public static class GameData {
        public static bool IsReady => GameController.IsReady;
        public static bool IsGameStart => GameController.IsGameStart;
        public static float GameTime => GameController.GameTime;
        public static Vector2[] EndPositions;
        public static float LongNoteInterval(float bpm) => 60 / bpm;
        public static float NoteArriveTime(float bpm) => 60 * GraceBeats / bpm / SpeedRate;
        public static float SpeedRate => Mathf.Clamp(NoteSpeed, 0.5f, 4f);
        public static float Offset => 0f;
        public static readonly int GraceBeats = 4; //猶予拍数
        public static float NoteSpeed { get; set; } = 1;
        public static float Bpm;
        public static float Score => GameController.ScoreManager.Score;
        public static int MaxScore => GameController.ScoreManager.MaxScore;
        public static int Combo => GameController.ScoreManager.ComboCount;
        public static int MaxCombo => GameController.ScoreManager.MaxComboCount;
        public static AudioClip CurrentMusic;
        public static (List<NotesData>, List<BPMData>) EditData;
    }
}
