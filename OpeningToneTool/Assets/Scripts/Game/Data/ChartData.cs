using System;
using System.Collections.Generic;
using System.Linq;
using Game.Note;
using Game.System;

namespace Game.Data {
    public class ChartData {
        public bool Equals(ChartData other) {
            return Bpm.Equals(other.Bpm)
                   && Time.Equals(other.Time)
                   && LaneNumber == other.LaneNumber;
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            return obj is ChartData other && Equals(other);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Bpm.GetHashCode();
                hashCode = (hashCode * 397) ^ Time.GetHashCode();
                hashCode = (hashCode * 397) ^ LaneNumber;
                return hashCode;
            }
        }

        public readonly float Bpm;
        public readonly float Time;
        public readonly int LaneNumber;
        public NoteType NoteType;
        public readonly float NoteLength;

        public ChartData(float bpm, float time, int laneNumber, NoteType noteType, float noteLength = 0) {
            Bpm = bpm;
            Time = time;
            LaneNumber = laneNumber;
            NoteType = noteType;
            NoteLength = noteLength;
        }

        public ChartData(ChartData data, NoteType noteType) {
            Bpm = data.Bpm;
            Time = data.Time;
            LaneNumber = data.LaneNumber;
            NoteType = noteType;
            NoteLength = data.NoteLength;
        }

        public void ChangeNoteTypeToSame() {
            NoteType = NoteType == NoteType.Normal
                ? NoteType.SameTimeNormal
                : NoteType == NoteType.Long
                    ? NoteType.SameTimeLong
                    : NoteType;
        }

        public static bool operator ==(ChartData data1, ChartData data2) {
            return Math.Abs(data1.Time - data2.Time) < 0.001f && data1.LaneNumber == data2.LaneNumber;
        }

        public static bool operator !=(ChartData data1, ChartData data2) {
            return !(data1 == data2);
        }

        public static (List<ChartData>, List<MusicBpmData>) GetChart((List<NotesData> notesDataList, List<BPMData> bpmDataList) data) {
            var chart = data.notesDataList.Select(x => new ChartData(x.bpm, x.time, x.lane, (NoteType) x.kind, x.length)).ToList();
            var bpm = data.bpmDataList.Where(x => x.bpmKind).Select(x => new MusicBpmData(x.bpm, x.bpmTime)).ToList();
            return (chart, bpm);
        }
    }
}
