using UnityEngine;

namespace Game.Data {
    [CreateAssetMenu]
    public class NoteDatabase : ScriptableObject {
        [SerializeField] private NoteData[] data;
        public NoteData[] Data => data;
    }
}
