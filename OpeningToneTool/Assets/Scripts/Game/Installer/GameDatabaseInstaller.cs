using Game.Data;
using UnityEngine;
using Zenject;

namespace Game.Installer {
    [CreateAssetMenu]
    public class GameDatabaseInstaller : ScriptableObjectInstaller<GameDatabaseInstaller> {
        public EvaluationDatabase EvaluationDatabase;
        public PositionDatabase PositionDatabase;
        public NoteDatabase NoteDatabase;

        public override void InstallBindings() {
            Container.BindInstance(EvaluationDatabase).IfNotBound();
            Container.BindInstance(PositionDatabase).IfNotBound();
            Container.BindInstance(NoteDatabase).IfNotBound();
        }
    }
}
