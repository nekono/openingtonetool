using System.Collections.Generic;

namespace Game.PauseSystem {
    public static class PauseManager {
        private static readonly List<IPausable> PauseList = new List<IPausable>();
        private static bool isPausing;

        public static void SwitchPause() {
            if (isPausing) {
                Resume();
            } else {
                Pause();
            }
        }

        public static void Pause() {
            isPausing = true;
            foreach (var pausable in PauseList) {
                pausable.Pause();
            }
        }

        public static void Resume() {
            isPausing = false;
            foreach (var pausable in PauseList) {
                pausable.Resume();
            }
        }

        public static void AddList(IPausable pausable) {
            PauseList.Add(pausable);
        }

        public static void RemoveList(IPausable pausable) {
            PauseList.Remove(pausable);
        }

        public static void Clear() {
            PauseList.Clear();
        }
    }
}
