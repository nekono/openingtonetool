namespace Game.PauseSystem {
    public interface IPausable {
        void Pause();
        void Resume();
    }
}
