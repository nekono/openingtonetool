using Game.Data;
using ProjectSystem;
using UnityEngine;

namespace Game.UI {
    public class DisplayScore : DisplayText {
        private int score;
        private bool once;

        private void Start() {
            score = Mathf.Clamp((int) GameData.Score, 0, GameData.MaxScore);
            Display();
        }

        private void Update() {
            if (!GameData.IsGameStart) return;
            SetScore();
            Display();
        }

        private void SetScore() {
            if (score != (int) GameData.Score) score += Mathf.Clamp(((int) GameData.Score - score) / 5, 1, (int) GameData.Score);
        }

        private void Display() {
            Display($"{score:0,000,000}");
            if (score >= 800000 && !once) {
                once = true;
                ChangeColor(new Color(251f / 255f, 255f / 255f, 81f / 255f));
            }
        }
    }
}
