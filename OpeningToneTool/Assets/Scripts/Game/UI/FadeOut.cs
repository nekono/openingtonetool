using System;
using Game.PauseSystem;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {
    public class FadeOut : MonoBehaviour, IPausable {
        [SerializeField] private float fadeTime = 0.5f;
        private Graphic graphic;
        private Color myColor;
        private bool isPausing;
        private readonly Subject<Unit> onClearColor = new Subject<Unit>();
        public IObservable<Unit> OnClearColor => onClearColor;

        private void Awake() {
            graphic = GetComponent<Graphic>();
            myColor = graphic.color;
            PauseManager.AddList(this);
        }

        private void OnEnable() {
            graphic.color = myColor;
        }

        private void Update() {
            if(isPausing) return;
            graphic.color -= Color.black * Time.deltaTime / fadeTime;
            if(graphic.color.a <= 0) onClearColor.OnNext(Unit.Default);
        }

        public void Pause() {
            isPausing = true;
        }

        public void Resume() {
            isPausing = false;
        }
    }
}
