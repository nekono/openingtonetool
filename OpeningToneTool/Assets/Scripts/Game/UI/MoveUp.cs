using Game.PauseSystem;
using UnityEngine;

namespace Game.UI {
    public class MoveUp : MonoBehaviour, IPausable {
        private RectTransform rectTransform;
        private bool isPausing;

        private void Awake() {
            rectTransform = GetComponent<RectTransform>();
            PauseManager.AddList(this);
        }

        private void Update() {
            if (isPausing) return;
            rectTransform.position += 2 * Vector3.up * Time.deltaTime;
        }

        public void Pause() {
            isPausing = true;
        }

        public void Resume() {
            isPausing = false;
        }
    }
}
