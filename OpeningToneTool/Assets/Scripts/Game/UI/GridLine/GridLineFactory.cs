using System.Linq;
using Game.Data;
using Game.ObjectPool;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.UI.GridLine {
    public class GridLineFactory : MonoBehaviour {
        [SerializeField] private GridLine gridLine;
        [Inject] private PositionDatabase posData;
        private float maxRadius;
        private ObjectPool<GridLine> pool;

        private void Awake() {
            pool = new ObjectPool<GridLine>(transform);
            pool.Preload(gridLine, 30);
            var temp = posData.Data[2];
            var rad = Vector3.Distance(temp.StartPosition, temp.EndPosition);
            maxRadius = rad * GetComponentInParent<Canvas>().referencePixelsPerUnit;
        }

        public void Create(float arrivalTime) {
            var obj = pool.GetObject(gridLine, transform.position);
            obj.Init(arrivalTime, maxRadius);
            obj.OnDestroy.First()
                .Subscribe(_ => pool.ReturnObject(obj))
                .AddTo(obj);
        }
    }
}
