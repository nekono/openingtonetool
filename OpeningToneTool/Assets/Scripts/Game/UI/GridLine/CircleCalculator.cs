using UnityEngine;

namespace Game.UI.GridLine {
    public static class CircleCalculator {
        private static int sideNum = 60;
        
        public static Vector2[] GetPositions(float radius, Vector2 center = new Vector2(), int num = 0) {
            num = num <= 2 ? sideNum : num;
            var pos = new Vector2[num+1];
            for (var i = 0; i < num+1; i++) {
                var amount = (float) i / num;
                var x = Mathf.Cos(2 * Mathf.PI * amount) * radius;
                var y = Mathf.Sin(2 * Mathf.PI * amount) * radius;
                pos[i] = center + Vector2.right * x + Vector2.up * y;
            }

            return pos;
        }
        
    }
}
