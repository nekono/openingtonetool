using System;
using Game.Data;
using ProjectSystem;
using UniRx;
using UnityEngine;

namespace Game.UI.GridLine {
    [RequireComponent(typeof(LineDrawer))]
    public class GridLine : MonoBehaviour {
        private float startTime;
        private float arrivalTime;
        private float maxRadius;
        private readonly CompositeDisposable disposable = new CompositeDisposable();
        private LineDrawer lineDrawer;
        private readonly Subject<Unit> onDestroy = new Subject<Unit>();
        public IObservable<Unit> OnDestroy => onDestroy;
        
        private void Awake() {
            lineDrawer = GetComponent<LineDrawer>();
        }

        public void Init(float arrivalTime, float maxRadius) {
            lineDrawer.SetPositions(CircleCalculator.GetPositions(0));
            startTime = GameData.GameTime;
            this.arrivalTime = arrivalTime;
            this.maxRadius = maxRadius;
            Observable.EveryUpdate()
                .Subscribe(_ => Move())
                .AddTo(gameObject)
                .AddTo(disposable);
        }

        private void Move() {
            var amount = (GameData.GameTime - startTime) / (arrivalTime - startTime);
            if(amount >= 1) Destroy();
            var radius = maxRadius * amount;
            var pos = CircleCalculator.GetPositions(radius);
            lineDrawer.SetPositions(pos);
        }

        private void Destroy() {
            disposable.Clear();
            onDestroy.OnNext(Unit.Default);
        }
    }
}
