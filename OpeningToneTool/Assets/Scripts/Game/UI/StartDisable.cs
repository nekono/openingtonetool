using Game.System;
using UnityEngine;

namespace Game.UI {
    public class StartDisable : MonoBehaviour, IGameStart {
        public void StartGame() {
            gameObject.SetActive(false);
        }
    }
}
