using Game.Data;
using UnityEngine;

namespace Game.UI {
    [DefaultExecutionOrder(1)]
    public class DisplayBpm : DisplayText{
        private void Start() {
            Display($"BPM : {GameData.Bpm}");
        }
    }
}
