using Game.Data;
using Game.System;
using UniRx.Async;
using UniRx.Async.Triggers;

namespace Game.UI {
    public class DisplayReady : DisplayText , IGameStart{
        private async void Start() {
            Setup().Forget();
        }

        private async UniTaskVoid Setup() {
            Display("");
            var cancel = this.GetCancellationTokenOnDestroy();
            await UniTask.WaitUntil(() => GameData.IsReady, cancellationToken: cancel);
            Display();
        }

        private void Display() {
            var text = "Ready";
            Display(text);
        }

        public void StartGame() {
            Display("");
        }
    }
}
