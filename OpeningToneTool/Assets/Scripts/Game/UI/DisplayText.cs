using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {
    [RequireComponent(typeof(Text))]
    public class DisplayText : MonoBehaviour {
        private Text text;

        protected virtual void Awake() {
            text = GetComponent<Text>();
            text.text = "";
        }

        public void Display(string message) {
            text.text = message;
        }

        public void ChangeColor(Color color) {
            text.color = color;
        }
    }
}
