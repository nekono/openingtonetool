using UnityEngine;
using UnityEngine.UI;

namespace Game.UI {
    public class DisplayGraphics : MonoBehaviour {
        [SerializeField] private Graphic graphic;

        private void Awake() {
            UnDisplay();
        }

        public void Display() {
            graphic.gameObject.SetActive(true);
        }

        public void UnDisplay() {
            graphic.gameObject.SetActive(false);
        }
    }
}
