using Game.Data;
using UnityEngine;

namespace Game.UI {
    public class DisplayCombo : MonoBehaviour {
        [SerializeField] private DisplayText comboDisplay;
        [SerializeField] private GameObject comboBackground;

        private int Combo => GameData.Combo;

        private void Start() {
            DisplayText();
        }

        private void Update() {
            DisplayText();
        }

        private void DisplayText() {
            var isCombo = Combo > 0;
            comboDisplay.gameObject.SetActive(isCombo);
            comboBackground.SetActive(isCombo);
            comboDisplay.Display($"{Combo}");
        }
    }
}
