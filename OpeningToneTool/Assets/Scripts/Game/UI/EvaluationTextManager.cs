using System;
using Game.Data;
using Game.ObjectPool;
using Game.PauseSystem;
using Game.System;
using UniRx;
using UniRx.Async;
using UniRx.Async.Triggers;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.UI {
    public class EvaluationTextManager : MonoBehaviour, IPausable {
        [SerializeField] private Image[] displays;
        [SerializeField] private Sprite wonderfulImage;
        [SerializeField] private Sprite greatImage;
        [SerializeField] private Sprite goodImage;
        [SerializeField] private Sprite missImage;
        private ObjectPool<Image> pool;
        private bool isPausing;
        [Inject] private PositionDatabase positionData;

        private void Awake() {
            PauseManager.AddList(this);
        }

        private void Start() {
            pool = new ObjectPool<Image>(displays, transform);
            var index = 0;
            var canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
            foreach (var display in displays) {
                var displayRect = display.GetComponent<RectTransform>();
                var screenPos = RectTransformUtility.WorldToScreenPoint(Camera.main, positionData.Data[index].EndPosition);
                RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRect, screenPos, Camera.main, out var pos);
                displayRect.localPosition = pos;
                index++;
                pool.Preload(display, 30);
            }
        }

        public async UniTaskVoid DisplayEvaluation(int laneNumber, BeatEvaluation evaluation) {
            var display = displays[laneNumber];
            var obj = pool.GetObject(display, display.transform.position);
            obj.sprite = evaluation == BeatEvaluation.WONDERFUL ? wonderfulImage
                : evaluation == BeatEvaluation.GREAT ? greatImage
                : evaluation == BeatEvaluation.GOOD ? goodImage
                : missImage;
            obj.SetNativeSize();
            var time = 0f;
            while (time < 0.5f) {
                await UniTask.Yield();
                if(isPausing) continue;
                time += Time.deltaTime;
            }

            pool.ReturnObject(obj);
        }

        public void Pause() {
            isPausing = true;
        }

        public void Resume() {
            isPausing = false;
        }
    }
}
