using Game.Data;
using ProjectSystem;

namespace Game.UI.Debugger {
    public class DisplayOffset  : DisplayText{
        private void Update() {
            Display($"Offset : {GameData.Offset}");
        }
    }
}
