using Game.Data;
using ProjectSystem;

namespace Game.UI.Debugger {
    public class DisplaySpeed : DisplayText{
        private void Update() {
            Display($"SpeedRate : {GameData.SpeedRate}");
        }
    }
}
