using Game.PauseSystem;
using ProjectSystem;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.UI {
    public class PauseButton : MonoBehaviour {
        [SerializeField] private Color pauseColor;
        [SerializeField] private Image fillImage;
        private Image image;
        private bool isPointDown;
        private bool isClicked => graceTime > 0;
        private bool isPausing;
        private bool canResume;
        private float graceTime;
        private float pointDownTime;

        private void Awake() {
            image = GetComponent<Image>();
        }

        public void OnPointDown() {
            isPointDown = true;
        }

        public void OnPointUp() {
            isPointDown = false;
            if (canResume) {
                canResume = false;
                SwitchPause();
            } else if (isPausing) {
                canResume = true;
            } else {
                pointDownTime = 0;
                graceTime = 3f;
            }
        }

        private void Update() {
            image.color = isClicked ? pauseColor : Color.white;
            fillImage.fillAmount = pointDownTime / 2f;
            if (!isClicked || isPausing) return;
            if (isPointDown) {
                pointDownTime += Time.deltaTime;
                if (pointDownTime >= 2f) {
                    graceTime = 0;
                    pointDownTime = 0;
                    SwitchPause();
                }
            } else {
                graceTime -= Time.deltaTime;
            }
        }

        public void SwitchPause() {
            isPausing = !isPausing;
            PauseManager.SwitchPause();
        }
    }
}
