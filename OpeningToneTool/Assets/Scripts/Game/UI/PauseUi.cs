using Game.PauseSystem;
using UnityEngine;

namespace Game.UI {
    public class PauseUi : MonoBehaviour, IPausable{
        private void Start() {
            PauseManager.AddList(this);
            gameObject.SetActive(false);
        }

        public void Pause() {
            gameObject.SetActive(true);
        }

        public void Resume() {
            gameObject.SetActive(false);
        }
    }
}
