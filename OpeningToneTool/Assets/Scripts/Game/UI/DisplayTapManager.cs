using Game.Data;
using Game.PauseSystem;
using Game.PlayerInput;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Game.UI {
    public class DisplayTapManager : MonoBehaviour, IPausable {
        [SerializeField] private Image[] displays;
        [Inject] private PlayerInputer input;
        [Inject] private PositionDatabase positionData;
        private bool isPausing;

        private void Awake() {
            PauseManager.AddList(this);
        }

        private void Start() {
            var index = 0;
            var canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
            foreach (var display in displays) {
                var displayRect = display.GetComponent<RectTransform>();
                var screenPos = RectTransformUtility.WorldToScreenPoint(Camera.main, positionData.Data[index].EndPosition);
                RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasRect, screenPos, Camera.main, out var pos);
                displayRect.localPosition = pos;
                index++;
            }
        }

        private void Update() {
            if (isPausing) return;
            var index = 0;
            foreach (var display in displays) {
                display.gameObject.SetActive(input.IsLaneTouching(index));
                index++;
            }
        }

        public void Pause() {
            isPausing = true;
        }

        public void Resume() {
            isPausing = false;
        }
    }
}
