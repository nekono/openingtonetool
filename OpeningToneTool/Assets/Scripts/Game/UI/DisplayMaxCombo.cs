using Game.Data;

namespace Game.UI {
    public class DisplayMaxCombo : DisplayText {
        private void Start() {
            Display(GameData.MaxCombo.ToString());
        }
    }
}
