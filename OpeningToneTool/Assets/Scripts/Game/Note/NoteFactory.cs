using System;
using System.Collections.Generic;
using System.Linq;
using Game.Data;
using Game.ObjectPool;
using UniRx;
using UnityEngine;
using Zenject;

namespace Game.Note {
    /// <summary>
    /// 各種ノーツをつくるよ
    /// ロングの実装がきたない
    /// </summary>
    public class NoteFactory : MonoBehaviour {
        [Inject] private NoteDatabase noteDatabase;
        [Inject] private PositionDatabase positionDatabase;
        private PositionData[] positionData => positionDatabase.Data;
        private ObjectPool<Note> pool;
        private readonly Subject<(int, NoteType, Note)> noteStream = new Subject<(int, NoteType, Note)>();
        public IObservable<(int, NoteType, Note)> OnCreateNote => noteStream;
        private readonly List<(ChartData, Note)> longDataList = new List<(ChartData, Note)>();
        private int minOrder = 102;
        private int order;

        private void Start() {
            GameData.EndPositions = positionData.Select(x => x.EndPosition).ToArray();
            pool = new ObjectPool<Note>(transform);
            foreach (var data in noteDatabase.Data) {
                pool.Preload(data.Note, 200);
            }
            order =  minOrder;
        }

        /// <summary>
        /// 通常生成
        /// </summary>
        public void Create(ChartData data) {
            var instance = noteDatabase.Data.First(x => x.NoteType == data.NoteType).Note;
            var posData = positionData[data.LaneNumber];
            Vector3 direction = posData.EndPosition - posData.StartPosition;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            //ノーツ画像上が正面なら90, 右なら0
            var forwardDiff = 180;
            var obj = pool.GetObject(instance, posData.StartPosition, Quaternion.AngleAxis(angle - forwardDiff, Vector3.forward));
            if (data.NoteType == NoteType.Long || data.NoteType == NoteType.SameTimeLong) {
                longDataList.Add((data, obj));
                var count = (int) (data.NoteLength / GameData.LongNoteInterval(data.Bpm));
                var times = new float[count];
                for (var i = 1; i <= count; i++) {
                    times[i - 1] = data.Time + GameData.LongNoteInterval(data.Bpm) * i;
                }

                (obj as LongNote).Init(data.Time, posData.EndPosition, times, order);
            } else {
                obj.Init(data.Time, posData.EndPosition,order);
            }

            order++;
            noteStream.OnNext((data.LaneNumber, data.NoteType, obj));
        }

        /// <summary>
        /// ロング用終端生成
        /// </summary>
        public void CreateLongEnd(ChartData data, ChartData longData) {
            var instance = noteDatabase.Data.First(x => x.NoteType == data.NoteType).Note;
            var posData = positionData[data.LaneNumber];
            Vector3 direction = posData.EndPosition - posData.StartPosition;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            //ノーツ画像上が正面なら90, 右なら0
            var forwardDiff = 180;
            var obj = pool.GetObject(instance, posData.StartPosition, Quaternion.AngleAxis(angle - forwardDiff, Vector3.forward));
            var temp = longDataList.First(x => x.Item1 == longData);
            var orderLayer = temp.Item2.SpriteRenderer.sortingOrder + 1;
            obj.Init(data.Time + data.NoteLength, posData.EndPosition, orderLayer);
            (temp.Item2 as LongNote)?.RegisterLongEnd(obj);
            longDataList.Remove(temp);
            noteStream.OnNext((data.LaneNumber, data.NoteType, obj));
        }

        public void DestroyNote(Note note) {
            pool.ReturnObject(note);
        }
    }
}
