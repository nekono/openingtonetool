using Game.Data;
using Game.System;
using UnityEngine;
using Zenject;

namespace Game.Note {
    public class NoteEvaluate : MonoBehaviour {
        [Inject] private EvaluationDatabase evaluationData;

        public (NoteType type, BeatEvaluation evaluation) EvaluateNote(NoteType type, Note note) {
            var range = GameData.GameTime - note.ArrivalTime;
            (NoteType type, BeatEvaluation evaluation) result = (type, BeatEvaluation.MISS);
            if (type == NoteType.Damage) {
                result.evaluation = range <= 0 ? BeatEvaluation.MISS : BeatEvaluation.WONDERFUL;
            } else {
                range = Mathf.Abs(range);
                result.evaluation = range <= evaluationData.WonderfulRange ? BeatEvaluation.WONDERFUL
                    : range <= evaluationData.GreatRange ? BeatEvaluation.GREAT
                    : range <= evaluationData.GoodRange ? BeatEvaluation.GOOD
                    : BeatEvaluation.MISS;
            }

            return result;
        }

        public bool IsMiss(Note note, bool isDamage = false) => isDamage ? IsMissDamage(note.ArrivalTime) : IsMiss(note.ArrivalTime);
        private bool IsMiss(float time) => (GameData.GameTime - time) > evaluationData.MissRange;
        private bool IsMissDamage(float time) => (GameData.GameTime - time) > 0f;
        public bool IsEvaluatable(Note note) => IsEvaluatable(note.ArrivalTime);
        private bool IsEvaluatable(float time) => Mathf.Abs(GameData.GameTime - time) <= evaluationData.MissRange;
        public bool IsAutoRange(Note note) => IsAutoRange(note.ArrivalTime);
        public bool IsAutoRange(float time) => Mathf.Abs(GameData.GameTime - time) <= evaluationData.AutoRange;
    }
}
