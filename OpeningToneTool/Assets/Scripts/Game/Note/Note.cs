using System;
using Game.Data;
using UniRx;
using UnityEngine;

namespace Game.Note {
    public class Note : MonoBehaviour {
        protected bool IsInitialized;
        protected float StartTime;
        private bool isPausing;
        private Vector2 startPos;
        private Vector2 endPos;
        protected float Length { get; private set; }
        public SpriteRenderer SpriteRenderer { get; private set; }
        protected IDisposable Disposable;
        protected readonly Subject<Unit> NoteDisable = new Subject<Unit>();
        public IObservable<Unit> OnNoteDisable => NoteDisable;
        public float ArrivalTime { get; private set; }

        private void Awake() {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void Init(float arrivalTime, Vector2 endPosition, int orderInLayer = 0) {
            if (IsInitialized) {
                Debug.LogWarning(this + " is already initialized!");
                return;
            }

            SpriteRenderer.sortingOrder = orderInLayer;
            StartTime = GameData.GameTime;
            ArrivalTime = arrivalTime;
            startPos = transform.position;
            endPos = endPosition;
            Length = (endPos - startPos).magnitude;
            Disposable = Observable.EveryUpdate()
                .Subscribe(_ => Move())
                .AddTo(gameObject);
            IsInitialized = true;
        }

        protected virtual void Move() {
            var amount = (GameData.GameTime - StartTime) / (ArrivalTime - StartTime);
            transform.position = startPos + (endPos - startPos) * amount;
        }

        protected virtual void OnDisable() {
            Disposable?.Dispose();
            IsInitialized = false;
            NoteDisable.OnNext(Unit.Default);
        }
    }
}
