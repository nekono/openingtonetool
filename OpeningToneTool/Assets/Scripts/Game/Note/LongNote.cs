using System.Linq;
using Game.Data;
using UniRx;
using UnityEngine;

namespace Game.Note {
    public class LongNote : Note {
        private float size = 3.01f;
        private Note longEndNote;
        private bool isCreatedEnd;
        public (bool isChecked, float time)[] EvaluationTimes { get; private set; }

        public void Init(float arrivalTime, Vector2 endPos, float[] evaluationTimes, int orderInLayer = 0) {
            EvaluationTimes = evaluationTimes.Select(x => (false, x)).ToArray();
            base.Init(arrivalTime, endPos, orderInLayer);
        }

        protected override void Move() {
            if (isCreatedEnd) {
                transform.position = longEndNote.transform.position;
            } else {
                var amount = (GameData.GameTime - StartTime) / (ArrivalTime - StartTime);
                SpriteRenderer.size = new Vector2(size / 2f + (Length * amount) / transform.localScale.x, SpriteRenderer.size.y);
            }
        }

        public void RegisterLongEnd(Note note) {
            longEndNote = note;
            isCreatedEnd = true;
            note.OnNoteDisable
                .First()
                .Subscribe(_ => NoteDisable.OnNext(Unit.Default));
        }

        private void OnEnable() {
            SpriteRenderer.size = Vector2.up * SpriteRenderer.size.y;
        }

        protected override void OnDisable() {
            Disposable?.Dispose();
            IsInitialized = false;
            isCreatedEnd = false;
        }
    }
}
