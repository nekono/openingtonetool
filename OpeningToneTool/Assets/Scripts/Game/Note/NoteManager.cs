using System;
using System.Collections.Generic;
using System.Linq;
using Game.PlayerInput;
using Game.System;
using Game.UI;
using UniRx;
using UnityEngine;
using Zenject;

namespace Game.Note {
    public class NoteManager : MonoBehaviour, IGameStart, IGameEnd {
        [Inject] private NoteFactory noteFactory;
        [Inject] private PlayerInputer playerInputer;
        [Inject] private EvaluationTextManager evaluationTextManager;

        private bool isActive;
        private NoteEvaluate noteEvaluate;
        private List<(int laneNumber, NoteType type, Note note)> notes;
        private List<(int laneNumber, LongNote note)> longNotes;
        private List<(int laneNumber, LongNote note)> longingNotes;

        private void Start() {
            noteEvaluate = GetComponent<NoteEvaluate>();
            notes = new List<(int, NoteType, Note)>();
            longNotes = new List<(int laneNumber, LongNote note)>();
            longingNotes = new List<(int laneNumber, LongNote note)>();
            Observable.EveryUpdate()
                .Where(_ => isActive)
                .Subscribe(_ => {
                    CheckMiss();
                    Long();
                })
                .AddTo(gameObject);
            Observable.EveryUpdate()
                .Where(_ => isActive)
                .Where(_ => playerInputer.IsAutoMode)
                .Subscribe(_ => AutoMode())
                .AddTo(gameObject);
            noteFactory.OnCreateNote
                .Subscribe(AddNote)
                .AddTo(gameObject);
            playerInputer.OnBeginTouch
                .Where(_ => isActive)
                .Subscribe(Beat)
                .AddTo(gameObject);
        }

        private void AddNote((int laneNumber, NoteType type, Note note) data) {
            switch (data.type) {
                case NoteType.Normal:
                case NoteType.SameTimeNormal:
                case NoteType.Effect:
                case NoteType.Damage:
                case NoteType.LongEnd:
                case NoteType.SameTimeLongEnd:
                    notes.Add((data.laneNumber, data.type, data.note));
                    break;
                case NoteType.Long:
                case NoteType.SameTimeLong:
                    var temp = (data.laneNumber, data.note as LongNote);
                    longNotes.Add(temp);
                    data.note.OnNoteDisable
                        .First()
                        .Subscribe(_ => {
                            noteFactory.DestroyNote(data.note);
                            if (!longingNotes.Remove(temp)) {
                                longNotes.Remove(temp);
                            }
                        });
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void RemoveNotes(List<(int laneNumber, NoteType type, Note note)> removeNotes) {
            foreach (var removeNote in removeNotes) {
                RemoveNote(removeNote);
            }
        }

        private void RemoveNote((int laneNumber, NoteType type, Note note) removeNote) {
            var (_, _, note) = removeNote;
            noteFactory.DestroyNote(note);
            var temp = notes.Find(x => x.note == note);
            notes.Remove(temp);
        }

        private void CheckMiss() {
            var list = notes
                .Where(x => noteEvaluate.IsMiss(x.note, x.type == NoteType.Damage))
                .ToList();
            if (list.Count <= 0) return;
            foreach (var (laneNumber, type, note) in list.Where(x => x.type != NoteType.LongEnd && x.type != NoteType.SameTimeLongEnd)) {
                var (noteType, evaluation) = noteEvaluate.EvaluateNote(type, note);
                GameController.ScoreManager.AddScore(evaluation);
                evaluationTextManager.DisplayEvaluation(laneNumber, evaluation).Forget();
            }

            RemoveNotes(list);
        }

        private void Beat(int laneNumber) {
            //単押しノーツ
            var results = notes
                .Where(x => x.type != NoteType.LongEnd)
                .Where(x => x.type != NoteType.SameTimeLongEnd)
                .Where(x => x.laneNumber == laneNumber)
                .Where(x => noteEvaluate.IsEvaluatable(x.note))
                .GroupBy(x => x.laneNumber)
                .Select(x => x.OrderBy(y => y.note.ArrivalTime).First())
                .ToList();
            if (results.Count > 0) {
                foreach (var n in results) {
                    var (type, evaluation) = noteEvaluate.EvaluateNote(n.type, n.note);
                    GameController.ScoreManager.AddScore(evaluation);
                    evaluationTextManager.DisplayEvaluation(n.laneNumber, evaluation).Forget();
                }

                RemoveNotes(results);
            }

            //ロングノーツ
            var longResults = longNotes
                .Where(x => x.laneNumber == laneNumber)
                .Where(x => noteEvaluate.IsEvaluatable(x.note) || noteEvaluate.IsMiss(x.note))
                .ToList();
            var hash = new HashSet<(int, LongNote)>(longResults);
            longNotes.RemoveAll(hash.Contains);
            if (longResults.Any()) {
                foreach (var n in longResults) {
                    longingNotes.Add(n);
                    var (type, evaluation) = noteEvaluate.EvaluateNote(NoteType.Long, n.note);
                    GameController.ScoreManager.AddScore(evaluation);
                    evaluationTextManager.DisplayEvaluation(n.laneNumber, evaluation).Forget();
                }
            }
        }

        private void Long() {
            if (!longingNotes.Any() && !longNotes.Any()) return;
            //まだ評価チェックしてないかつチェックすべきものを抽出
            var checkable = longNotes
                .Concat(longingNotes)
                .Where(x => x.note.EvaluationTimes.Any(y => !y.isChecked && noteEvaluate.IsAutoRange(y.time)))
                .ToList();
            if (checkable.Any()) {
                foreach (var (laneNumber, note) in checkable) {
                    var o = note.EvaluationTimes;
                    for (int i = 0; i < o.Length; i++) {
                        if (o[i].isChecked || !noteEvaluate.IsAutoRange(o[i].time)) continue;
                        o[i].isChecked = true;
                        var evaluation = (playerInputer.IsLaneTouching(laneNumber) || playerInputer.IsAutoMode)
                            ? BeatEvaluation.WONDERFUL
                            : BeatEvaluation.MISS;
                        AddScore(laneNumber, evaluation);
                    }
                }
            }
        }

        private void AutoMode() {
            var results = notes
                .Where(x => x.type != NoteType.LongEnd)
                .Where(x => x.type != NoteType.SameTimeLongEnd)
                .Where(x => x.type != NoteType.Damage)
                .Where(x => noteEvaluate.IsAutoRange(x.note))
                .ToList();
            if (results.Count > 0) {
                foreach (var n in results) {
                    var (_, evaluation) = noteEvaluate.EvaluateNote(n.type, n.note);
                    AddScore(n.laneNumber, evaluation);
                }

                RemoveNotes(results);
            }

            //ロングノーツ
            var longResults = longNotes
                .Where(x => noteEvaluate.IsAutoRange(x.note))
                .GroupBy(x => x.laneNumber)
                .Select(x => x.OrderBy(y => y.note.ArrivalTime).First())
                .ToList();
            var hash = new HashSet<(int, LongNote)>(longResults);
            longNotes.RemoveAll(hash.Contains);

            if (longResults.Count > 0) {
                foreach (var n in longResults) {
                    longingNotes.Add(n);
                    var (_, evaluation) = noteEvaluate.EvaluateNote(NoteType.Long, n.note);
                    AddScore(n.laneNumber, evaluation);
                }
            }

            var temp = new bool[6];
            for (var i = 0; i < temp.Length; i++) {
                temp[i] = results.Any(x => x.laneNumber == i) || longResults.Any(x => x.laneNumber == i) || longingNotes.Any(x => x.laneNumber == i);
            }

            playerInputer.ApplyAutoTap(temp);
        }
        private void AddScore(int laneNumber, BeatEvaluation evaluation) {
            GameController.ScoreManager.AddScore(evaluation);
            evaluationTextManager.DisplayEvaluation(laneNumber, evaluation).Forget();
        }

        public void EndGame() {
            isActive = false;
        }

        public void StartGame() {
            isActive = true;
        }
    }
}
