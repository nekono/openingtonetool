namespace Game.Note {
    public enum NoteType {
        Normal = 0,
        Effect = 1,
        Long = 2,
        Damage = 3,
        LongEnd = 4,
        SameTimeNormal,
        SameTimeLong,
        SameTimeLongEnd
    }
}
