using System;
using System.Diagnostics;
using System.Linq;
using Game.Data;
using Game.PauseSystem;
using Game.System;
using UniRx;
using UnityEngine;

namespace Game.PlayerInput {
    public class PlayerInputer : MonoBehaviour, IPausable,IGameStart, IGameEnd {
        [SerializeField] private InputMode inputMode;
        private readonly float range = 15; //入力範囲 判定位置からの距離
        private readonly Subject<int> laneTouchDown = new Subject<int>();
        public IObservable<int> OnBeginTouch => laneTouchDown;
        private readonly bool[] isLaneTouching = new bool[6];
        private readonly CompositeDisposable[] disposables = new CompositeDisposable[6];
        public bool IsLaneTouching(int laneNumber) => isLaneTouching[laneNumber];

        private bool isAutoMode;
        public bool IsAutoMode => isAutoMode;
        public void SetAutoMode(bool value) => isAutoMode = value;
        private bool isPausing;
        private bool isActive;


        private void Awake() {
            PauseManager.AddList(this);
        }

        private void Start() {
            for (var i = 0; i < disposables.Length; i++) {
                disposables[i] = new CompositeDisposable();
            }

            Init();
        }

        private void Init() {
#if UNITY_ANDROID
            inputMode = InputMode.Tap;
#else
            inputMode = InputMode.Button;
#endif
        }

        private void Update() {
            if (isPausing || !isActive) return;
            inputMode = isAutoMode ? InputMode.Auto : inputMode;
            switch (inputMode) {
                case InputMode.Click:
                    GetClick();
                    break;
                case InputMode.Button:
                    GetButton();
                    GetClick();
                    break;
                case InputMode.Tap:
                    GetTap();
                    break;
                case InputMode.Auto:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void GetClick() {
            if (Input.GetMouseButtonDown(0)) {
                var (laneNumber, distance) = TouchToLaneNumber(Input.mousePosition);
                if (distance > range) return;
                laneTouchDown.OnNext(laneNumber);
                for (var index = 0; index < isLaneTouching.Length; index++) {
                    isLaneTouching[index] = laneNumber == index;
                }
            } else if (Input.GetMouseButton(0)) {
                var (laneNumber, distance) = TouchToLaneNumber(Input.mousePosition);
                if (distance > range) return;
                for (var index = 0; index < isLaneTouching.Length; index++) {
                    isLaneTouching[index] = laneNumber == index;
                }
            } else {
                for (var index = 0; index < isLaneTouching.Length; index++) {
                    isLaneTouching[index] = false;
                }
            }
        }

        private void GetButton() {
            var buttonDowns = new[] {
                Input.GetKeyDown(KeyCode.Keypad7),
                Input.GetKeyDown(KeyCode.Keypad9),
                Input.GetKeyDown(KeyCode.Keypad6),
                Input.GetKeyDown(KeyCode.Keypad3),
                Input.GetKeyDown(KeyCode.Keypad1),
                Input.GetKeyDown(KeyCode.Keypad4),
            };
            var buttons = new[] {
                Input.GetKey(KeyCode.Keypad7),
                Input.GetKey(KeyCode.Keypad9),
                Input.GetKey(KeyCode.Keypad6),
                Input.GetKey(KeyCode.Keypad3),
                Input.GetKey(KeyCode.Keypad1),
                Input.GetKey(KeyCode.Keypad4),
            };
            var list = buttonDowns
                .Select((onButton, index) => (onButton, index))
                .Where(x => x.onButton)
                .ToList();
            foreach (var (_, index) in list) {
                laneTouchDown.OnNext(index);
            }

            for (var index = 0; index < isLaneTouching.Length; index++) {
                isLaneTouching[index] = buttons[index];
            }
        }

        private void GetTap() {
            var touches = Input.touches;
            if (touches.Length > 0) {
                //全タッチ解析→有効タッチのみ排出→レーン被り排除(長押し優先)
                var list = touches
                    .Select(x => (touch: x, TouchToLaneNumber(x)))
                    .Where(x => x.Item2.distance <= range)
                    .GroupBy(x => x.Item2.index)
                    .Select(x => x.OrderByDescending(y => y.touch.phase).First())
                    .Select(x => (x.touch, x.Item2.index))
                    .ToList();

                foreach (var (touch, index) in list) {
                    switch (touch.phase) {
                        case TouchPhase.Began:
                            laneTouchDown.OnNext(index);
                            break;
                        case TouchPhase.Moved:
                        case TouchPhase.Stationary:
                        case TouchPhase.Ended:
                        case TouchPhase.Canceled:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                for (var index = 0; index < isLaneTouching.Length; index++) {
                    isLaneTouching[index] = list.Any(x => x.index == index);
                }
            } else {
                for (var index = 0; index < isLaneTouching.Length; index++) {
                    isLaneTouching[index] = false;
                }
            }
        }

        public void ApplyAutoTap(bool[] laneTouching) {
            for (var i = 0; i < 6; i++) {
                if (laneTouching[i]) {
                    isLaneTouching[i] = laneTouching[i];
                    disposables[i].Clear();
                    var j = i;
                    Observable.Timer(TimeSpan.FromSeconds(0.15f))
                        .First()
                        .Subscribe(_ => isLaneTouching[j] = false)
                        .AddTo(disposables[j]);
                }
            }
        }

        private (int index, float distance) TouchToLaneNumber(Touch touch) {
            var pos = (Vector2) Camera.main.ScreenToWorldPoint(touch.position);
            var distances = GameData.EndPositions.Select(x => (x - pos).sqrMagnitude).ToArray();
            return (Array.IndexOf(distances, distances.Min()), distances.Min());
        }

        private (int index, float distance) TouchToLaneNumber(Vector2 position) {
            var pos = (Vector2) Camera.main.ScreenToWorldPoint(position);
            var distances = GameData.EndPositions.Select(x => (x - pos).sqrMagnitude).ToArray();
            return (Array.IndexOf(distances, distances.Min()), distances.Min());
        }

        private enum InputMode {
            Click,
            Button,
            Tap,
            Auto
        }

        public void Pause() {
            isPausing = true;
        }

        public void Resume() {
            isPausing = false;
        }

        public void StartGame() {
            isActive = true;
        }

        public void EndGame() {
            isActive = false;
        }
    }
}
