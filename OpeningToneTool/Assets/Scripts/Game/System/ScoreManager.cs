using System;

namespace Game.System {
    public class ScoreManager {
        private readonly int maxCount;
        private readonly int maxScore;
        private int hitCount;
        private float HitRate => hitCount / 10f / maxCount;

        public int MaxCount => maxCount;
        public int MaxScore => maxScore;
        public float Score => maxScore * HitRate;
        public int WonderfulCount { get; private set; }
        public int GreatCount { get; private set; }
        public int GoodCount { get; private set; }
        public int MissCount { get; private set; }
        public int ComboCount { get; private set; }
        private int maxComboCount;
        public int MaxComboCount => ComboCount > maxComboCount ? ComboCount : maxComboCount;
        public bool IsClear => Score >= 800000;
        public bool IsFullCombo => Score >= maxScore;

        public ScoreManager(int maxCount,int maxScore = 1000000) {
            this.maxCount = maxCount;
            this.maxScore = maxScore;
        }

        public void AddScore(BeatEvaluation grade) {
            hitCount += (int) grade;
            switch (grade) {
                case BeatEvaluation.WONDERFUL:
                    WonderfulCount++;
                    ComboCount++;
                    break;
                case BeatEvaluation.GREAT:
                    GreatCount++;
                    ComboCount++;
                    break;
                case BeatEvaluation.GOOD:
                    GoodCount++;
                    ComboCount++;
                    break;
                case BeatEvaluation.MISS:
                    MissCount++;
                    if (maxComboCount < ComboCount) maxComboCount = ComboCount;
                    ComboCount = 0;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(grade), grade, null);
            }
        }
    }
}
