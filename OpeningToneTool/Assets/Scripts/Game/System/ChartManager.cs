using System.Collections.Generic;
using System.Linq;
using Game.Data;
using Game.Note;
using Game.UI.GridLine;
using UniRx;
using UnityEngine;
using Zenject;

namespace Game.System {
    public class ChartManager : MonoBehaviour, IGameStart {
        [Inject] private NoteFactory noteFactory;
        [Inject] private GridLineFactory gridLineFactory;
        private List<ChartData> chart;
        private List<MusicBpmData> musicBpm;
        private bool isInitialized;
        private int gridNum;
        private float nextGridTime;

        public int GetScoreCount =>
            chart.Count
            + chart.Where(x => x.NoteType == NoteType.Long || x.NoteType == NoteType.SameTimeLong)
                .Sum(x => (int) (x.NoteLength / GameData.LongNoteInterval(x.Bpm)));

        public void Init() {
            if (isInitialized) {
                Debug.LogError(this + " is already initialized!");
                return;
            }

            (chart, musicBpm) = ChartData.GetChart(GameData.EditData);
            chart = chart.OrderBy(x => x.Time).ToList();
            isInitialized = true;
        }

        public void StartGame() {
            Observable.EveryUpdate()
                .Subscribe(_ => ObserveChart())
                .AddTo(gameObject);
            Observable.EveryUpdate()
                .Subscribe(_ => ManageGridLine())
                .AddTo(gameObject);
        }

        private void ObserveChart() {
            while (chart.Count > 0 && GameData.GameTime >= chart[0].Time - GameData.NoteArriveTime(chart[0].Bpm)) {
                var data = chart[0];
                noteFactory.Create(data);
                if (data.NoteType == NoteType.Long || data.NoteType == NoteType.SameTimeLong) {
                    //終端用のデータコピー
                    var noteType = data.NoteType == NoteType.Long ? NoteType.LongEnd : NoteType.SameTimeLongEnd;
                    var end = new ChartData(data, noteType);
                    //時間がいい感じになったら生成
                    Observable.EveryUpdate()
                        .First(__ => GameData.GameTime >= data.Time - GameData.NoteArriveTime(data.Bpm) + data.NoteLength)
                        .Subscribe(___ => noteFactory.CreateLongEnd(end, data))
                        .AddTo(gameObject);
                }
                chart.RemoveAt(0);
            }
            
            while (musicBpm.Count > 0 && GameData.GameTime >= musicBpm[0].Time) {
                GameData.Bpm = musicBpm[0].Bpm;
                musicBpm.RemoveAt(0);
            }
        }

        private void ManageGridLine() {
            if (GameData.GameTime >= nextGridTime - GameData.NoteArriveTime(GameData.Bpm)) {
                gridLineFactory.Create(nextGridTime);
                gridNum++;
                nextGridTime = gridNum * 4 * 60f / GameData.Bpm;
            }
        }
    }
    
    public struct MusicBpmData {
        public readonly float Bpm;
        public readonly float Time;

        public MusicBpmData(float bpm, float time) {
            Bpm = bpm;
            Time = time;
        }
    }
}
