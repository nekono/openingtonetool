using System;
using System.Collections.Generic;
using Game.Data;
using Game.Note;
using Game.PauseSystem;
using ProjectSystem;
using UniRx;
using UniRx.Async;
using UniRx.Async.Triggers;
using UnityEngine;
using Zenject;

namespace Game.System {
    [DefaultExecutionOrder(-10)]
    public class GameController : MonoBehaviour, IPausable {
        [Inject] private ChartManager chartManager;
        [Inject] private MusicPlayer musicPlayer;
        [Inject] private NoteManager noteManager;

        private readonly BoolReactiveProperty isPausing = new BoolReactiveProperty(false);
        private List<IGameStart> starters = new List<IGameStart>();
        private float startTime;
        private float pauseTime;

        public static bool IsGameStart { get; private set; }
        public static bool IsReady { get; private set; }
        public static float GameTime { get; private set; }
        public static ScoreManager ScoreManager { get; private set; } = new ScoreManager(0);

        private void Start() {
            PauseManager.AddList(this);
            starters = new List<IGameStart>();
            foreach (var n in FindObjectsOfType<Component>()) {
                if (n is IGameStart starter) {
                    starters.Add(starter);
                }
            }

            chartManager.Init();
            musicPlayer.SetMusic(GameData.CurrentMusic);

            ScoreManager = new ScoreManager(chartManager.GetScoreCount);

            Observable.EveryUpdate()
                .Where(_ => IsGameStart)
                .Where(_ => !isPausing.Value)
                .Subscribe(_ => GameTime = Time.time - startTime - pauseTime)
                .AddTo(gameObject);

            StartGame().Forget();
        }

        private async UniTaskVoid StartGame()
        {
            if (IsGameStart) return;
            IsGameStart = true;
            var cancel = this.GetCancellationTokenOnDestroy();
            await UniTask.Delay(TimeSpan.FromSeconds(1), cancellationToken: cancel);
            IsReady = true;
            await UniTask.Delay(TimeSpan.FromSeconds(2), cancellationToken: cancel);
            await UniTask.WaitUntil(() => !isPausing.Value, cancellationToken: cancel);
            foreach (var starter in starters)
            {
                starter.StartGame();
            }

            startTime = Time.time + GameData.Offset + GameData.NoteArriveTime(GameData.Bpm);
            pauseTime = 0;

            musicPlayer.OnEndMusic
                .First()
                .Subscribe(_ => EndGame())
                .AddTo(gameObject);
        }

        private void EndGame(bool isGameOver = false) {
            if (!IsGameStart) return;
            IsGameStart = false;
            var ends = new List<IGameEnd>();
            foreach (var n in FindObjectsOfType<Component>()) {
                if (n is IGameEnd end) {
                    ends.Add(end);
                }
            }

            SceneManager.UnloadScene(Scenes.Preview).Forget();
        }

        private void OnDisable() {
            IsGameStart = false;
            IsReady = false;
        }

        public void QuitGame() {
            PauseManager.Resume();
            SceneManager.UnloadScene(Scenes.Preview).Forget();
        }

        public void Pause() {
            isPausing.Value = true;
            Observable.EveryUpdate()
                .TakeUntil(isPausing.Where(x => !x))
                .Subscribe(_ => pauseTime += Time.deltaTime)
                .AddTo(gameObject);
        }

        public void Resume() {
            isPausing.Value = false;
        }
    }
}
