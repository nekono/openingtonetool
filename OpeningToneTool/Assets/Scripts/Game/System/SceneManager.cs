using Game.PauseSystem;
using UniRx.Async;
using UnityEngine.SceneManagement;

namespace Game.System {
    public static class SceneManager {
        private static bool isSceneChanging;

        public static async UniTaskVoid SceneMove(Scenes scene, bool isAdditive = true) {
            if (isSceneChanging) return;
            isSceneChanging = true;
            PauseManager.Clear();
            await UnityEngine.SceneManagement.SceneManager.LoadSceneAsync((int) scene, isAdditive ? LoadSceneMode.Additive : LoadSceneMode.Single);
            isSceneChanging = false;
        }

        public static async UniTaskVoid UnloadScene(Scenes scene) {
            if (isSceneChanging) return;
            isSceneChanging = true;
            PauseManager.Clear();
            await UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync((int) scene);
            isSceneChanging = false;
        }
    }

    public enum Scenes {
        Editor, Preview
    }
}
