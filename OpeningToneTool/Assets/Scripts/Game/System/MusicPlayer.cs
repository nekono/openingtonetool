using System;
using Game.Data;
using Game.PauseSystem;
using Game.System;
using UniRx;
using UniRx.Async;
using UniRx.Async.Triggers;
using UnityEngine;

namespace ProjectSystem {
    [RequireComponent(typeof(AudioSource))]
    public class MusicPlayer : MonoBehaviour, IGameStart, IPausable {
        private AudioSource source;
        private readonly Subject<Unit> endMusic = new Subject<Unit>();
        public IObservable<Unit> OnEndMusic => endMusic;
        private bool isPausing;

        private void Awake() {
            source = GetComponent<AudioSource>();
            PauseManager.AddList(this);
        }

        public void SetMusic(AudioClip clip) {
            source.clip = clip;
        }

        public void StartGame() => PlayGameMusic().Forget();

        private async UniTaskVoid PlayGameMusic() {
            var cancel = this.GetCancellationTokenOnDestroy();
            await UniTask.Delay(TimeSpan.FromSeconds(GameData.NoteArriveTime(GameData.Bpm)), cancellationToken: cancel);
            await UniTask.WaitUntil(() => !isPausing, cancellationToken: cancel);
            Play(false);
        }

        public void Play(bool canLoop = true) {
            source.loop = canLoop;
            source.Play();
            Observable.EveryUpdate()
                .Where(_ => !isPausing)
                .First(_ => !source.isPlaying)
                .Subscribe(_ => endMusic.OnNext(Unit.Default))
                .AddTo(source);
        }

        public void Pause() {
            isPausing = true;
            source.Pause();
        }

        public void Resume() {
            source.UnPause();
            isPausing = false;
        }
    }
}
