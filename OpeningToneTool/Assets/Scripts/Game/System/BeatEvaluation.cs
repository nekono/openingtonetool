using System;

namespace Game.System {
    public enum BeatEvaluation {
        WONDERFUL = 10, GREAT = 8, GOOD = 6, MISS = 0
    }

    public static class BeatEvaluationExtend {
        public static float NormalDamage(this BeatEvaluation param) {
            switch (param) {
                case BeatEvaluation.WONDERFUL:
                    return 0.4f;
                case BeatEvaluation.GREAT:
                    return 0.2f;
                case BeatEvaluation.GOOD:
                    return 0f;
                case BeatEvaluation.MISS:
                    return -5f;
                default:
                    throw new ArgumentOutOfRangeException(nameof(param), param, null);
            }
        }
        public static float LongDamage(this BeatEvaluation param) {
            switch (param) {
                case BeatEvaluation.WONDERFUL:
                    return 0.2f;
                case BeatEvaluation.GREAT:
                case BeatEvaluation.GOOD:
                    return 0f;
                case BeatEvaluation.MISS:
                    return -2f;
                default:
                    throw new ArgumentOutOfRangeException(nameof(param), param, null);
            }
        }
        public static float Damage(this BeatEvaluation param) {
            switch (param) {
                case BeatEvaluation.WONDERFUL:
                    return 0.5f;
                case BeatEvaluation.GREAT:
                case BeatEvaluation.GOOD:
                    return 0f;
                case BeatEvaluation.MISS:
                    return -10f;
                default:
                    throw new ArgumentOutOfRangeException(nameof(param), param, null);
            }
        }
    }
}