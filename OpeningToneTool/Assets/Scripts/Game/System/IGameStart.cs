namespace Game.System {
    public interface IGameStart {
        void StartGame();
    }
}
