using UnityEngine;

namespace Game.System.Debugger {
    public class ResetButton : MonoBehaviour {
        public void ResetGame() {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
}
