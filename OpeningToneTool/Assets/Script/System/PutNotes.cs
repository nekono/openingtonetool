﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Linq;
using UnityEngine.Rendering;


public class PutNotes : MonoBehaviour
{
    [SerializeField] public NotesKindButton notesKindButton;

    [SerializeField] public Material normalMaterial;
    [SerializeField] public Mesh normalMesh;

    [SerializeField] public Material longStartMaterial;
    [SerializeField] public Mesh longStartMesh;

    [SerializeField] public Material longMiddleMaterial;
    [SerializeField] public Mesh longMiddleMesh;

    [SerializeField] public Material longEndMaterial;
    [SerializeField] public Mesh longEndMesh;

    [SerializeField] public Material ngMaterial;
    [SerializeField] public Mesh ngMesh;

    [SerializeField]
    GridSetting gridSetting;
    [SerializeField]
    NotesSpeedSetting notesSpeedSetting;
    [SerializeField]
    SoundManager soundManager;

    [SerializeField] SystemText systemText;

    [SerializeField]
    BeatLine beatLine;

    [SerializeField]
    GameObject settingUI;

    [SerializeField]
    SaveConfirmatior saveConfirmatior;

    public bool isDrawYet = false;

    public float longStartTime = -1f;
    public NotesData longEditingData = new NotesData();       //NotesDrawerでエンドノーツ関連の表示に使用
    public bool longEndSetting = false;     //ロングノーツの終端の設定中かどうか
    int drawTiming = 0;                     //カーソルの点滅用

    Vector2 initPos = new Vector3(-5.25f, -0.9f, 0f);   //円形の中心位置


    void Start()
    {
        //ノーツのメッシュの処理
        normalMesh.RecalculateNormals();
        normalMesh.RecalculateBounds();

        longStartMesh.RecalculateNormals();
        longStartMesh.RecalculateBounds();

        longMiddleMesh.RecalculateNormals();
        longMiddleMesh.RecalculateBounds();

        longEndMesh.RecalculateNormals();
        longEndMesh.RecalculateBounds();

        ngMesh.RecalculateNormals();
        ngMesh.RecalculateBounds();
    }

    //カーソルの表示と設置
    public void DrawNotesCursor(Vector3 cursorPos, Quaternion angle, float time, int lane, float bpm)
    {
        cursorPos.z = 93f;
        if (Input.GetMouseButton(1)) { drawTiming = 0; }

        if (Vector2.Distance(initPos, cursorPos) < 10.8f && isDrawYet == false)
        {
            if (drawTiming > 25)
            {
                switch (notesKindButton.notesKind)
                {
                    case (int)NotesKind.Normal:
                        Graphics.DrawMesh(normalMesh, cursorPos, angle, normalMaterial, 0);
                        break;

                    case (int)NotesKind.Sound:
                        break;

                    case (int)NotesKind.Long:
                        if (longEndSetting == false) { Graphics.DrawMesh(longStartMesh, cursorPos, angle, longStartMaterial, 0); }
                        else { Graphics.DrawMesh(longEndMesh, cursorPos, angle, longEndMaterial, 0); }
                        break;

                    case (int)NotesKind.NG:
                        Graphics.DrawMesh(ngMesh, cursorPos, angle, ngMaterial, 0);
                        break;

                    default:
                        break;
                }
            }

            isDrawYet = true;
            if (drawTiming > 50) { drawTiming = 0; }
            drawTiming++;

            //ノーツを設置する(左クリック)
            if (Input.GetMouseButtonDown(0) && settingUI.activeSelf == false){ PutNotesData(time, lane, bpm); }
        }
    }


    //ノーツデータの設置
    public void PutNotesData(float time, int lane, float bpm)
    {
        //string putData;
        NotesData putData = new NotesData();

        //編集用の一時的なノーツのみのデータ(Time,Lane,Kind,Length,BPM)に追加
        switch (notesKindButton.notesKind)
        {
            //ノーマルノーツの設置
            case (int)NotesKind.Normal:
                putData.time = time;
                putData.lane = lane;
                putData.kind = notesKindButton.notesKind;
                putData.length = 0;
                putData.bpm = bpm;
                if (NotesDataManager.isCanPutNotes(putData)) { NotesDataManager.editNotesData.Add(putData); }
                NotesDataManager.editNotesDataTimeSort();   //ソートさせる
                saveConfirmatior.isNotSaved = true;
                break;

            //ロングノーツの設置
            case (int)NotesKind.Long:
                //仮のデータで置けるかどうかを確かめる
                putData.time = time;
                putData.lane = lane;
                putData.kind = notesKindButton.notesKind;
                putData.length = 0;
                putData.bpm = bpm;
                if (NotesDataManager.isCanPutNotes(putData))
                {
                    //終端を未設定の場合
                    if (longEndSetting == true && time != longStartTime)
                    {
                        //ロングノーツの先端から終端までの時間を計算し、データを追加する
                        putData.time = longStartTime;
                        putData.lane = lane;
                        putData.kind = notesKindButton.notesKind;
                        putData.length = time - longStartTime;
                        putData.bpm = bpm;

                        if (putData.bpm * putData.length / 60.0f < 1.0f)
                        {
                            systemText.ShowSystemWarning("注意：ロングノーツの長さが適正値より短いです。");
                        }

                        //ロングノーツの中間が四分音符1回分の長さがあるか
                        if ((60 / bpm) <= putData.length + 1)
                        {
                            longStartTime = -1f;
                            longEndSetting = false;
                            NotesDataManager.editNotesData.Add(putData);
                            saveConfirmatior.isNotSaved = true;
                            NotesDataManager.editNotesDataTimeSort();   //ソートさせる
                        }
                        break;
                    }

                    //先端の情報を保持し、終端設定に移る
                    if (longEndSetting == false)
                    {
                        longStartTime = time;
                        longEditingData.time = longStartTime;
                        longEditingData.lane = lane;
                        longEditingData.kind = notesKindButton.notesKind;
                        longEditingData.length = 0;
                        longEditingData.bpm = bpm;
                        longEndSetting = true;
                    }
                }
                break;

            //NGノーツの設置
            case (int)NotesKind.NG:
                putData.time = time;
                putData.lane = lane;
                putData.kind = notesKindButton.notesKind;
                putData.length = 0;
                putData.bpm = bpm;
                if (NotesDataManager.isCanPutNotes(putData)) { NotesDataManager.editNotesData.Add(putData); }
                saveConfirmatior.isNotSaved = true;
                NotesDataManager.editNotesDataTimeSort();   //ソートさせる
                break;

            default:
                break;
        }
    }

    //ノーツデータの削除
    public void DeleteNotesData(float time, int lane, float bpm)
    {
        int index = 0;

        foreach (var editData in NotesDataManager.editNotesData)
        {
            //時間とレーンが一致するものがあった場合
            if (time == editData.time && lane == editData.lane)
            {
                NotesDataManager.editNotesData.RemoveAt(index); //ノーツデータの削除
                NotesDataManager.editNotesDataTimeSort();   //ソートさせる
                break;
            }
            index++;
        }
        saveConfirmatior.isNotSaved = true;
    }
}
