﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class NotesDrawer : MonoBehaviour
{
    //ノーツが出てくる位置、向かう先、角度を使うためにBeatLineを取得
    [SerializeField] BeatLine beatLine;

    //Meshとマテリアルを使用するためにPutNotesを取得
    [SerializeField] PutNotes putNotes;

    [SerializeField] GridSetting gridSetting;

    [SerializeField] SoundManager soundManager;

    [SerializeField] Toggle effectToggle;

    public AudioClip effect;
    AudioSource audioEffect;
    private float onPlayedEffect;
    private HashSet<float> playedEffect = new HashSet<float>();


    public List<NotesData> deleteNotesStack = new List<NotesData>();


    private void Start()
    {
        audioEffect = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //効果音を一回だけ鳴らすためのやつを初期化
        if(soundManager.isPlaying() == false && playedEffect.Count != 0)
        {
            playedEffect.Clear();
        }


        //ロングノーツの先端だけ置いた場合
        if (putNotes.longEndSetting)
        {
            float time = putNotes.longEditingData.time;
            int lane = putNotes.longEditingData.lane;
            int kind = putNotes.longEditingData.kind;
            float length = putNotes.longEditingData.length;
            float bpm = putNotes.longEditingData.bpm;

            float initTime = 60 * beatLine.beat / (bpm * beatLine.rate * beatLine.choiceBeat);     //タイミングバーまで何秒必要か
            if (gridSetting.isGridFrame) { initTime = gridSetting.slider.value; }    //グリッドがフレームで固定の場合

            Quaternion angle = Quaternion.Euler(0f, 0f, beatLine.purposeAngle[lane]);
            Vector3 pos = beatLine.initPos + (beatLine.purposePos[lane] - beatLine.initPos) * ((soundManager.playTime - time + initTime) / initTime);
            pos.z = 94f;

            if (Vector2.Distance(beatLine.initPos, pos) < 14f) { Graphics.DrawMesh(putNotes.longStartMesh, pos, angle, putNotes.longStartMaterial, 0); }
        }


        //編集用の一時的なノーツのみのデータ(Time,Lane,Kind,Length,BPM)からノーツを描画
        foreach (var editData in NotesDataManager.editNotesData)
        {
            bool audio = false;
            float time = editData.time;
            int lane = editData.lane;
            int kind = editData.kind;
            float length = editData.length;
            float bpm = editData.bpm;

            float initTime = 60 * beatLine.beat / (bpm * beatLine.rate * beatLine.choiceBeat);     //タイミングバーまで何秒必要か
            if (gridSetting.isGridFrame) { initTime = gridSetting.slider.value; }    //グリッドがフレームで固定の場合

            if (time - initTime < soundManager.playTime)
            {
                Quaternion angle = Quaternion.Euler(0f, 0f, beatLine.purposeAngle[lane]);
                Vector3 pos = beatLine.initPos + (beatLine.purposePos[lane] - beatLine.initPos) * ((soundManager.playTime - time + initTime) / initTime);
                pos.z = 94f;


                //マウスのポジションをとる
                Vector3 position = Input.mousePosition;
                position.z = 10f;
                Vector3 movePos = Camera.main.ScreenToWorldPoint(position);
                //マウスカーソルと一定距離近い状態のノーツを対象とする
                if (Vector2.Distance(movePos,pos) < 0.5f)
                {
                    //マウスの右クリックで消すノーツをスタック
                    if (Input.GetMouseButtonDown(1))
                    {
                        NotesData data = new NotesData();
                        data.bpm = bpm;
                        data.lane = lane;
                        data.time = time;
                        deleteNotesStack.Add(data);
                    }
                }

                //効果音を鳴らす
                if (time <= soundManager.playTime + 0.03f && time >= soundManager.playTime - 0.01f && soundManager.isPlaying() && effectToggle.isOn)
                {
                    if (playedEffect.Contains(time) == false)
                    {
                        audioEffect.PlayOneShot(effect);
                        playedEffect.Add(time);
                    }
                    
                }

                //一定の距離以内であれば描画する
                switch (kind)
                {
                    //ノーマルノーツ
                    case (int)NotesKind.Normal:
                        if (Vector2.Distance(beatLine.initPos, pos) < 14f) { Graphics.DrawMesh(putNotes.normalMesh, pos, angle, putNotes.normalMaterial, 0); }
                        break;

                    //効果音ノーツ
                    case (int)NotesKind.Sound:
                        break;


                    case (int)NotesKind.Long:
                        if (Vector2.Distance(beatLine.initPos, pos) < 14f) { Graphics.DrawMesh(putNotes.longStartMesh, pos, angle, putNotes.longStartMaterial, 0); }

                        //ロングノーツの長さが0でなく、始点と終点の間の長さによって中間を表示
                        if (length != 0.0f)
                        {
                            Vector3 endPos = beatLine.initPos + (beatLine.purposePos[lane] - beatLine.initPos) * ((soundManager.playTime - time - length + initTime) / initTime);

                            //中間ノーツを細かく配置していく
                            float addTime = 60 * beatLine.beat / (bpm * beatLine.rate * 32);
                            if (gridSetting.isGridFrame) { addTime = gridSetting.slider.value / 18; }    //グリッドがフレームで固定の場合
                            for (float i = time; i <= time + length; i += addTime)
                            {
                                float drawTime = i;
                                Vector3 middlePos = beatLine.initPos + (beatLine.purposePos[lane] - beatLine.initPos) * ((soundManager.playTime - drawTime + initTime) / initTime);

                                if ((drawTime - initTime) < soundManager.playTime)
                                {
                                    middlePos.z = 95f;
                                    if (Vector2.Distance(beatLine.initPos, middlePos) < 14f) { Graphics.DrawMesh(putNotes.longMiddleMesh, middlePos, angle, putNotes.longMiddleMaterial, 0); }
                                }
                            }
                        }

                        //ロングノーツの長さが0でなく、表示可能時間以上で終端を表示
                        if (length != 0 && (time + length - initTime) < soundManager.playTime)
                        {
                            Vector3 endPos = beatLine.initPos + (beatLine.purposePos[lane] - beatLine.initPos) * ((soundManager.playTime - time - length + initTime) / initTime);
                            endPos.z = 94f;
                            if (Vector2.Distance(beatLine.initPos, endPos) < 14f) { Graphics.DrawMesh(putNotes.longEndMesh, endPos, angle, putNotes.longEndMaterial, 0); }
                        }
                        break;

                    case (int)NotesKind.NG:
                        if (Vector2.Distance(beatLine.initPos, pos) < 14f) { Graphics.DrawMesh(putNotes.ngMesh, pos, angle, putNotes.ngMaterial, 0); }
                        break;

                    default:
                        break;
                }
            }
        }


        //おそらく速度的に１つのノーツしか削除対象に追加されてないが念のためforeachで回してノーツを削除していく
        if (deleteNotesStack.Count > 0)
        {
            foreach (var item in deleteNotesStack)
            {
                putNotes.DeleteNotesData(item.time, item.lane, item.bpm);
            }
            deleteNotesStack.Clear();   //削除リストクリア
        }

    }
}
