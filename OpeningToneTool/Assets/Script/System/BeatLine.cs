﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeatLine : MonoBehaviour
{
    [SerializeField]
    Material[] material;
    [SerializeField] Mesh mesh;

    [SerializeField]
    SoundManager soundManager;

    [SerializeField]
    GridSetting gridSetting;
    [SerializeField]
    NotesSpeedSetting notesSpeedSetting;

    [SerializeField]
    PutNotes putNotes;

    [SerializeField]
    Toggle lineMode;

    public Vector2 initPos = new Vector3(-5.25f, -0.9f,0f);   //ラインの初期位置
    public Vector2[] purposePos = new Vector2[]            //目的先の位置(0~5)
    {
        new Vector2(-11.9f,4.787f),
        new Vector2(1.4f,4.787f),
        new Vector2(3.500113f,-0.9f),
        new Vector2(1.4f,-6.587f),
        new Vector2(-11.9f,-6.587f),
        new Vector2(-14.00011f,-0.9f),
    };

    //ノーツが向かう先の角度
    public float[] purposeAngle = new float[]
    {
        139.5f, 40.5f, 0f, -40.5f, -139.5f, 180f,
    };

    float note = 4;     //設置するノーツの間隔(4,8,16,32)


    public int beat = 128;       //速度が最大の時、4拍で到達する(UI等からの変更は現状できない)

    public float choiceBeat = 1; //4,8,16,32(1,2,4,8)

    public float rate = 1.0f;  //ユーザー指定速度




    // Start is called before the first frame update
    void Start()
    {     
        
        mesh = new Mesh();
        mesh.vertices = new Vector3[]
        {
            new Vector3(-0.02f,-1.3f,0),   //左下
            new Vector3(-0.02f, 1.2f,0),   //左上
            new Vector3( 0.02f,-1.3f,0),   //右下
            new Vector3( 0.02f, 1.2f,0),   //右上
        };

        mesh.uv = new Vector2[]
        {
            new Vector2(0,0),
            new Vector2(0,1),
            new Vector2(1,0),
            new Vector2(1,1),
        };

        mesh.triangles = new int[]
        {
            0,1,2,
            1,3,2,
        };
        
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = Input.mousePosition;
        position.z = 10f;

        Vector3 movePos = Camera.main.ScreenToWorldPoint(position);


        int countBPMData = 0;
        rate = notesSpeedSetting.slider.value;
        float initTime = 0;     //タイミングバーまで何秒必要か
        int countLine = 8;  //グリッド線を4,8,16,32で出すためのやつ
        int beatCount = 0;  //4,8,16,32で色を分けるために数えてる
        int colorNo = 0;    //色(マテリアル)の番号
        bool moveMusicTime = false; //マウスの中央ボタンで再生位置を移動する用

        foreach (var x in BPMUnitManager.bpmTimeList)
        {
            //BPMユニットの最後の要素は曲の最後までLineを表示するため
            if (x.bpmTime == BPMUnitManager.bpmTimeList[BPMUnitManager.bpmTimeList.Count - 1].bpmTime)
            {
                if (gridSetting.isGridFrame){ initTime = gridSetting.slider.value; }    //グリッドがフレームで固定の場合
                else { initTime = 60 * beat / (x.bpm * rate * choiceBeat); }

                float addTime = (60 / (x.bpm * 8));

                //自分が持つBPM変化する時間 ; 曲の最後まで ; 4分音符の場合の次のLine表示タイミング(1つの音符の時間)
                for (float i = x.bpmTime; i <= soundManager.MusicLength(); i = i + addTime)
                {
                    //4,8,16,32で色を分けるための処理
                    if ((beatCount % 8) == 0.0f) { colorNo = 0; }
                    else if ((beatCount % 4) == 0.0f) { colorNo = 1; }
                    else if ((beatCount % 2) == 0.0f) { colorNo = 2; }
                    else { colorNo = 3; }
                    if(beatCount >= 32) { beatCount = 0; }
                    
                    //表示時間以上になったら表示
                    if (soundManager.playTime - i + initTime >= 0)
                    {
                        for (int lane = 0; lane < 6; lane++)
                        {
                            Quaternion angle = Quaternion.Euler(0f, 0f, purposeAngle[lane]);
                            Vector3 pos = initPos + (purposePos[lane] - initPos) * ((soundManager.playTime - i + initTime) / initTime);
                            pos.z = 96f;

                            if (lineMode.isOn && Mathf.Log(choiceBeat, 2) == colorNo || lineMode.isOn == false)
                            {
                                if (countLine >= (8 / choiceBeat))
                                {
                                    //一定の距離以内であれば描画する
                                    if (Vector2.Distance(initPos, pos) < 14f) { Graphics.DrawMesh(mesh, pos, angle, material[colorNo], 0); }

                                    //マウスの真ん中のボタンでそのラインに飛ぶ
                                    if (Vector2.Distance(movePos, pos) < 0.5f && Input.GetMouseButtonUp(2) && moveMusicTime == false)
                                    {
                                        soundManager.UpdateMusicTime(i);
                                        moveMusicTime = true;
                                    }

                                    //ラインに重なっているときにノーツのカーソルを表示させる
                                    if (Vector2.Distance(movePos, pos) < 0.5f) { putNotes.DrawNotesCursor(pos, angle, i, lane, x.bpm); }
                                }
                            }
                        }
                    }
                    if (countLine >= (8 / choiceBeat)) { countLine = 0; }
                    countLine++;
                    beatCount++;
                }
            }

            //BPMユニットから次の変化タイミングまでLineを表示
            else if (countBPMData < BPMUnitManager.bpmTimeList.Count - 1)
            {
                if (gridSetting.isGridFrame) { initTime = gridSetting.slider.value; }    //グリッドがフレームで固定の場合
                else { initTime = 60 * beat / (x.bpm * rate * choiceBeat); }

                float addTime = (60 / (x.bpm * 8));

                //自分が持つBPM変化する時間 ; 次のBPMのユニットが持つ時間まで ; 4分音符の場合の次のLine表示タイミング(1つの音符の時間)
                for (float i = x.bpmTime; i < BPMUnitManager.bpmTimeList[countBPMData + 1].bpmTime /*+ initTime*/; i = i + addTime)
                {
                    //4,8,16,32で色を分けるための処理
                    if ((beatCount % 8) == 0.0f) { colorNo = 0; }
                    else if ((beatCount % 4) == 0.0f) { colorNo = 1; }
                    else if ((beatCount % 2) == 0.0f) { colorNo = 2; }
                    else { colorNo = 3; }
                    if (beatCount >= 32) { beatCount = 0; }

                    //表示時間以上になったら表示
                    if (soundManager.playTime - i + initTime >= 0)
                    {
                        for (int lane = 0; lane < 6; lane++)
                        {
                            Quaternion angle = Quaternion.Euler(0f, 0f, purposeAngle[lane]);
                            Vector3 pos = initPos + (purposePos[lane] - initPos) * ((soundManager.playTime - i + initTime) / initTime);
                            pos.z = 96f;

                            if (lineMode.isOn && Mathf.Log(choiceBeat, 2) == colorNo || lineMode.isOn == false)
                            {
                                if (countLine >= (8 / choiceBeat))
                                {
                                    //一定の距離以内であれば描画する
                                    if (Vector2.Distance(initPos, pos) < 14f) { Graphics.DrawMesh(mesh, pos, angle, material[colorNo], 0); }

                                    //マウスの真ん中のボタンでそのラインに飛ぶ
                                    if (Vector2.Distance(movePos, pos) < 0.5f && Input.GetMouseButtonUp(2) && moveMusicTime == false)
                                    {
                                        soundManager.UpdateMusicTime(i);
                                        moveMusicTime = true;
                                    }

                                    //ラインに重なっているときにノーツのカーソルを表示させる
                                    if (Vector2.Distance(movePos, pos) < 0.5f) { putNotes.DrawNotesCursor(pos, angle, i, lane, x.bpm); }
                                }
                            }
                        }
                    }
                    if (countLine >= (8 / choiceBeat)) { countLine = 0; }
                    countLine++;
                    beatCount++;
                }
            }
            countBPMData++;
            putNotes.isDrawYet = false;
        }
    }
}
