﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//終了前にセーブしたかどうか判定する
public class SaveConfirmatior : MonoBehaviour
{
    public bool isMusicLoaded = false;
    public bool isNotSaved = false;

    [SerializeField]
    GameObject saveCautionPanel;


    private void OnApplicationQuit()
    {
        if (isMusicLoaded && isNotSaved)
        {
            if(saveCautionPanel.activeSelf == false)
            {
                Debug.Log("Not Save");
                Application.CancelQuit();
                saveCautionPanel.SetActive(true);

            }
        }
    }

}
