﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class NotesData
{
    //(Time, Lane, Kind, Length, BPM)
    public float time;
    public int lane;
    public int kind;
    public float length;
    public float bpm;
}

public enum NotesKind
{
    Normal,
    Sound,
    Long,
    NG
}



public static class NotesDataManager
{
    [SerializeField]
    public static List<string> notesData = new List<string>();  //ノーツデータ(編集中ではほぼ使わない)

    public static List<NotesData> editNotesData = new List<NotesData>();  //編集用の一時的なノーツのみのデータ(Time,Lane,Kind,Length,BPM)

    public static int notesDifficult;

    public static bool notesCountUpdate = false;

    //ノーツデータの初期化
    public static void NotesDataInit(string musicName)
    {
        notesDifficult = 1;

        notesData.Clear();

        notesData.Add(musicName + ",,,,");      //音楽のタイトル
        notesData.Add("notes editor,,,,");      //譜面作者名
        notesData.Add("music editor,,,,");      //音楽制作者名
        notesData.Add("BPM,120,0,,");           //BPM,BPMの値,そのBPMになる時間

        editNotesData.Clear();                  //編集用のノーツデータのクリア

    }

    //BPMのデータを追加する
    public static void BPMAdder(float bpmTime, float bpm, bool bpmKind)
    {
        notesData.Add(string.Format("BPM,{0},{1},,", bpm, bpmTime, bpmKind));   //ノーツデータにBPMを追加

        notesCountUpdate = true;
    }

    //引数の時間のBPMを削除する
    public static void BPMDataDelete(float bpmTime, float bpm, bool bpmKind)
    {
        string item = string.Format("BPM,{0},{1},,", bpm, bpmTime);
        //BPMとBPMの変更時間を探し出す
        int index = notesData.IndexOf(item);

        notesData.RemoveAt(index);      //該当箇所を削除する

        notesCountUpdate = true;
    }

    //引数の時間のBPMを変更する
    public static void BPMDataChanger(float bpmTime, float beforeBPM, float afterBPM)
    {
        string item = string.Format("BPM,{0},{1},,", beforeBPM, bpmTime);
        //BPMとBPMの変更時間を探し出す
        int index = notesData.IndexOf(item);

        notesData[index] = string.Format("BPM,{0},{1},,", afterBPM, bpmTime);   //全体のデータのBPMを変更する

        notesCountUpdate = true;
    }

    //BPMが変更されたときにすでに設置されているエディタ用のノーツのBPMを変更する
    //BPMUnitManager.bpmTimeListのindexから時間変更があったBPMUnit自身の時間と、そのあとのBPMUnitまでの時間のノーツのBPMを変更する(Unitが1個しかない場合を除く)
    public static void EditNotesBPMChanfger(int index)
    {
        //最後のBPMUnit(もしくは唯一の)が変更された場合
        if(BPMUnitManager.bpmTimeList.Count-1 != index)
        {
            foreach (NotesData data in editNotesData)
            {
                //ノーツのデータの中にBPM変化に該当するノーツがあった
                if(data.time >= BPMUnitManager.bpmTimeList[index].bpmTime && data.time < BPMUnitManager.bpmTimeList[index + 1].bpmTime)
                {
                    var changeIndex = editNotesData.FindIndex(n => n.time == data.time && n.lane == data.lane);
                    editNotesData[changeIndex].bpm = BPMUnitManager.bpmTimeList[index].bpm;
                }
            }
        }
        //最後のBPMUniy(もしくは唯一の)ではないが変更されたとき
        else
        {
            foreach (NotesData data in editNotesData)
            {
                //ノーツのデータの中にBPM変化に該当するノーツがあった
                if (data.time >= BPMUnitManager.bpmTimeList[index].bpmTime)
                {
                    var changeIndex = editNotesData.FindIndex(n => n.time == data.time && n.lane == data.lane);
                    editNotesData[changeIndex].bpm = BPMUnitManager.bpmTimeList[index].bpm;
                }
            }
        }
    }





    //編集用の一時的なノーツのみのデータ(Time,Lane,Kind,Length,BPM)の時間とレーンの被りがないかを確かめる(true で　置くことが出来る)
    public static bool isCanPutNotes(NotesData data)
    {

        foreach (var editData in editNotesData)
        {
            //時間とレーンが一致するものがあった場合
            if(data.time == editData.time && data.lane == editData.lane)
            {
                return false;
            }

            //ロングノーツの中間に入ってしまった場合
            //ノーツの種類が=="Long"  &&  編集しているレーンが一致  && ロングノーツの先端と時間が同じかそれ以上  &&  ロングノーツの終端までの時間と同じかそれ以下
            if(editData.kind == (int)NotesKind.Long && data.lane == editData.lane && data.time >= editData.time && data.time <= editData.time + editData.length)
            {
                return false;
            }

            //ロングノーツの終端とかぶってしまってもダメ
            //編集しているレーンが一致  && ロングノーツの終端の時間と同じか
            if (data.lane == editData.lane && data.time == editData.time + editData.length)
            {
                return false;
            }
        }

        return true;
    }

    //編集用の一時的なノーツのみのデータ(Time,Lane,Kind,Length,BPM)を時間で昇順ソートする
    public static void editNotesDataTimeSort()
    {
        editNotesData = editNotesData.OrderBy(x => x.time).ToList();

        notesCountUpdate = true;
    }
    


}
