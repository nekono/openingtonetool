﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BPMData
{
    public float bpmTime { get; set; }
    public float bpm { get; set; }
    public bool bpmKind { get; set; }  //曲のBPM(trueで曲自体のBPM変化を示唆)
}


public static class BPMUnitManager
{

    [SerializeField]
    public static List<BPMData> bpmTimeList = new List<BPMData>();  //編集用のBPMのみのデータ


    //譜面用BPMの変化タイミングの時間をリストに追加
    public static void AddBPMTime(float timeData, float bpmData, bool kind)
    {
        var data = new BPMData();
        data.bpmTime = timeData;
        data.bpm = bpmData;
        data.bpmKind = kind;
        bpmTimeList.Add(data);
        SortBPMList();
    }

    //BPM変化タイミングを削除
    public static void DeleteBPMTime(float timeData, float bpmData, bool bpmKind)
    {
        var index = bpmTimeList.FindIndex(m => m.bpmTime == timeData);
        bpmTimeList.RemoveAt(index);
    }


    //BPM変化のタイミングに重複がないように既に存在しているかを確認
    public static bool SearchListBPMTime(float timeData)
    {
        var index = bpmTimeList.FindIndex(m => m.bpmTime == timeData);

        if(index == -1)
        {
            //Listに存在しない
            return false;
        }
        else
        {
            //既に存在している
            return true;
        }
    }


    //昇順にソート
    public static void SortBPMList()
    {
        bpmTimeList = bpmTimeList.OrderBy(x => x.bpmTime).ToList();
    }
}
