﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameDataManager : MonoBehaviour
{

    public static GameDataManager gameDataManagerInstance;

    [SerializeField]
    public string path;             //編集中のデータの親フォルダパス
    public string musicPath;        //曲のパス
    public int difficult;           //難易度
    public string musicName;        //曲名
    public string notesEditor;      //譜面作者
    public string comporser;        //作曲者
    public int bpm;                 //曲で使う初期のBPM

    public bool onReady;            //音楽ファイルが読み込まれ、再生の準備が出来ているか

 
    //シングルトン化
    private void Awake()
    {
        if (gameDataManagerInstance == null)
        {
            gameDataManagerInstance = this;
            DontDestroyOnLoad(gameObject);
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }


    //変数の初期化
    private void Init()
    {
        path = null;
        difficult = 1;
        musicName = "Music";
        notesEditor = "unknown";
        comporser = "unknown";
        bpm = 120;
    }

}
