﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Unityの解像度バグ？に対応するための解像度強制変更
public class ScreenSizeChanger : MonoBehaviour
{
    private void Awake()
    {
        Screen.SetResolution(1024, 576, false);
    }
}
