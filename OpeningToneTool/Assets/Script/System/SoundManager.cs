﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public bool onPlaying;                  //曲が再生中が可能か
    public AudioSource source;
    public float playTime;

    [SerializeField]
    UITimeManager uITimeManager;

    [SerializeField]
    MusicNotesInfoManager musicNotesInfoManager;

    [SerializeField]
    MenuBar menuBar;

    [SerializeField]
    Slider Volume;

    private void Awake()
    {
        if (AttachSoundManager == null)
        {
            AttachSoundManager = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        source = GetComponent<AudioSource>();
    }


    //曲を受け取る
    public void ReceiveMusic(string path)
    {
        source.time = 0.0f;
        playTime = 0.0f;
        StartCoroutine("LoadSound", path);
    }

    //曲の長さの取得
    public float MusicLength()
    {
        if(source == null)
        {
            return 0.0f;
        }
        if(source.clip == null)
        {
            return 0.0f;
        }
        if(source.clip.length == 0.0f)
        {
            return 0.0f;
        }
        return source.clip.length;
    }

    //曲の再生
    public void StartMusic()
    {
        source.time = playTime;
        onPlaying = true;
        source.Play();
    }

    //曲の停止
    public void StopMusic()
    {
        playTime = source.time;
        onPlaying = false;
        source.Stop();
    }

    //再生中か否か
    public bool isPlaying()
    {
        return source.isPlaying;
    }


    //アップデート
    private void Update()
    {
        source.volume = Volume.value;


        if (Input.GetKey(KeyCode.UpArrow)){ playTime += 0.01f; }
        if (Input.GetKey(KeyCode.DownArrow)) { playTime -= 0.01f; }
        if (Input.GetKey(KeyCode.RightArrow)) { playTime += 0.1f; }
        if (Input.GetKey(KeyCode.LeftArrow)) { playTime -= 0.1f; }

        if(Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            playTime += Input.GetAxis("Mouse ScrollWheel");
        }

        //曲が終わったら停止する
        if (playTime > MusicLength()){ StopMusic(); }

        if (playTime < 0) { playTime = 0; }

        if (onPlaying){ playTime = source.time; }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            // 読み込んだファイルを再生・停止する
            if (!source.isPlaying)
            {
                StartMusic();
            }
            else
            {
                StopMusic();
            }
        }
    }

    //スクリプトでこれをアタッチする用
    public static SoundManager AttachSoundManager { get; private set; }


    //再生/停止ボタンからの再生と停止
    public void PlayStop()
    {
        // 読み込んだファイルを再生・停止する
        if (!source.isPlaying)
        {
            StartMusic();
        }
        else
        {
            StopMusic();
        }
    }


    //時間の変更
    public void UpdateMusicTime(float inputTime)
    {
        if(inputTime < 0.0f)
        {
            inputTime = 0.0f;
        }
        if (inputTime > source.clip.length)
        {
            inputTime = source.clip.length;
            uITimeManager.ChangeUITextTime();
            StopMusic();
        }

        source.time = inputTime;
        playTime = inputTime;
    }


    //曲の読み込みとアタッチ
    private IEnumerator LoadSound(string path)
    {
        // ファイルが無かったら終わる
        if (!System.IO.File.Exists(path))
        {
            Debug.Log("File does NOT exist!! file path = " + path);
            yield return 0;
            //yield break;
        }

        // 指定したファイルをロードする
        WWW request = new WWW("file://" + path);

        // ロードが終わるまで待つ
        while (!request.isDone)
        {
            yield return new WaitForEndOfFrame();
        }

        // 読み込んだファイルからAudioClipを取り出す
        AudioClip audioTrack = request.GetAudioClip(false, true);
        while (audioTrack.loadState == AudioDataLoadState.Loading)
        {
            // ロードが終わるまで待つ
            yield return new WaitForEndOfFrame();
        }

        if (audioTrack.loadState != AudioDataLoadState.Loaded)
        {
            // 読み込み失敗
            Debug.Log("Failed to Load!");
            yield break;
        }

        // 生成したsourceに読み込んだAudioClicpを設定する
        source.clip = audioTrack;

        musicNotesInfoManager.isLoadedMusic();  //UIの曲名,作曲者,ノーツ制作者,難易度を入力可能にする
        menuBar.isReadySaveExport();            //saveとexportを有効にする
        GameDataManager.gameDataManagerInstance.onReady = true;     //準備完了を知らせる


        yield return 0;
    }
}
