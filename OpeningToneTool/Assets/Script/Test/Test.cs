﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SFB;

public class Test : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var extensions = new[]
        {
            new ExtensionFilter("WAV","WAV")
        };

        var path = StandaloneFileBrowser.OpenFilePanel("Open Music File", "", extensions, true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
