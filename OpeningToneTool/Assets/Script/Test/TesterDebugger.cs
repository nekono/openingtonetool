﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesterDebugger : MonoBehaviour
{
    public void ShowNotesData()
    {
        foreach (NotesData x in NotesDataManager.editNotesData)
        {
            //(Time,Lane,Kind,Length,BPM)
            Debug.Log(string.Format("Edit:{0},{1},{2},{3},{4}", x.time, x.lane, x.kind, x.length, x.bpm));
        }
    }

    public void ImportData()
    {
        for (int i = 0; i < NotesDataManager.editNotesData.Count; i++)
        {
            if (NotesDataManager.editNotesData[i].kind >= 1)
            {
                NotesDataManager.editNotesData[i].kind += 1;
            }
        }
    }

    public void BPMDataView()
    {
        foreach (BPMData x in BPMUnitManager.bpmTimeList)
        {
            Debug.Log(x.bpmTime + ":" + x.bpm);
        }
    }
}
