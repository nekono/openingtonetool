﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class BPMUnit : MonoBehaviour
{
    [SerializeField]
    InputField bpmInputField;
    [SerializeField]
    SoundManager soundManager;

    [SerializeField]
    public float bpm;               //BPM

    [SerializeField]
    public float bpmTime;           //BPMが変化する時間

    [SerializeField]
    public bool bpmKind;            //BPMの種類(?)(true = 曲のBPM, false = 譜面のBPM)

    RectTransform rect;
    float initY;                    //生成時の初期のY座標


    // Script Execution OrderでSoundManagerより遅くAwakeが呼ばれるように調整している
    void Awake()
    {
        //SoundManagerをアタッチ、自分自身の時間を取得
        soundManager = SoundManager.AttachSoundManager;
        rect = GetComponent<RectTransform>();
        initY = rect.localPosition.y;
    }

    //最初のユニットのための関数
    public void FirstUnit()
    {
        bpmTime = soundManager.playTime;
        bpm = 120f;
        bpmKind = true;
        bpmInputField.text = "120";
        BPMUnitManager.AddBPMTime(bpmTime, bpm, bpmKind);       //ListにBPMとBPM変化の時間を登録
        NotesDataManager.BPMAdder(bpmTime, bpm, bpmKind);       //BPMをノーツデータに追加
        rect = GetComponent<RectTransform>();
        initY = rect.localPosition.y;
    }

    //データオープン時の最初のユニットのための関数
    public void OpenFirstUnit(string setBPM, string time, string kind)
    {
        bpmTime = float.Parse(time);
        bpm = float.Parse(setBPM);
        bpmInputField.text = setBPM;

        if(kind == "True"){
            bpmKind = true;
        }
        else{
            bpmKind = false;
        }

        BPMUnitManager.AddBPMTime(bpmTime, bpm, bpmKind);       //ListにBPMとBPM変化の時間を登録
        NotesDataManager.BPMAdder(bpmTime, bpm, bpmKind);       //BPMをノーツデータに追加
        rect = GetComponent<RectTransform>();
        initY = rect.localPosition.y;
    }

    //自身が生成されたときにBPMInputterからBPMの値を受け取り、セットする
    public void SetBPM(float setBPM, float time, bool kind)
    {
        bpm = setBPM;
        bpmTime = time;
        bpmInputField.text = bpm.ToString();
        bpmKind = kind;

        BPMUnitManager.AddBPMTime(bpmTime, bpm, kind);       //ListにBPMとBPM変化の時間を登録
        NotesDataManager.BPMAdder(time, bpm, bpmKind);          //BPMをノーツデータに追加

        var index = BPMUnitManager.bpmTimeList.FindIndex(n => n.bpmTime == bpmTime);
        NotesDataManager.EditNotesBPMChanfger(index);
    } 


    //InputFieldeからBPMを入力し、変更する
    public void InputBPM()
    {
        float beforeBPM = bpm;                          //更新前のBPMの値
        bpm = float.Parse(bpmInputField.text);
        var index = BPMUnitManager.bpmTimeList.FindIndex(n => n.bpmTime == bpmTime);
        BPMUnitManager.bpmTimeList[index].bpm = bpm;    //BPMの値を更新

        NotesDataManager.BPMDataChanger(bpmTime, beforeBPM, bpm);  //ノーツデータのBPMを変更する
        NotesDataManager.EditNotesBPMChanfger(index);
    }


    //Deleteボタンで自身を削除
    public void OnPush()
    {
        var index = BPMUnitManager.bpmTimeList.FindIndex(n => n.bpmTime == bpmTime);
        BPMUnitManager.DeleteBPMTime(bpmTime, bpm, bpmKind);     //自分のもつBPM変化する時間をBPMデータリストから削除
        NotesDataManager.BPMDataDelete(bpmTime, bpm, bpmKind);   //ノーツデータからBPMを削除

        NotesDataManager.EditNotesBPMChanfger(index - 1);

        Destroy(this.gameObject);                       //自分を削除
    }


    //自分自身の移動
    private void Update()
    {
        Vector3 pos = rect.localPosition;
        float y = (bpmTime - soundManager.playTime) * 30 + initY;
        pos.y = y;
        rect.localPosition = pos;
    }
}
