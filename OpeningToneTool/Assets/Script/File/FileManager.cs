﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ファイルのパスの操作系統(区切る,ファイル名のみの取得など)

public static class FileManager
{
    //全体のパスを取得
    public static string ReadPath(string[] paths)
    {
        if (paths.Length == 0)
        {
            return null;
        }

        string _path = "";
        foreach (var p in paths)
        {
            _path += p;
        }
        return _path;
    }


    //パスで区切る
    public static string[] CutPath(string paths)
    {
        if (paths.Length == 0)
        {
            return null;
        }

        string[] _path;
        _path = paths.Split(System.IO.Path.DirectorySeparatorChar);
        return _path;
    }


    //ファイル名を取得する
    public static string GetFilePath(string[] paths)
    {
        var file = ReadPath(paths);
        if (file == null)
        {
            return null;
        }
        else
        {
            //var path = CutPath(file);
            //return path[path.Length - 1];           
            return System.IO.Path.GetFileName(file);
            
        }
    }
}
