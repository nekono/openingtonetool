﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToEditor : MonoBehaviour
{
    [SerializeField]
    GameObject saveCautionPanel;

    public void onPushBackButton()
    {
        saveCautionPanel.SetActive(false);
    }
}
