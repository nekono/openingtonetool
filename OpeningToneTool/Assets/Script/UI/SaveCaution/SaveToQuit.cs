﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveToQuit : MonoBehaviour
{
    [SerializeField]
    SaveButton saveButton;

    public void onPushSaveToQuit()
    {
        saveButton.SaveEditData();
        Application.Quit();
    }
}
