﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemText : MonoBehaviour
{

    //システムからの通知を表示する
    [SerializeField]
    Text systemText;

    public Vector3 initPos;
    public Vector3 movePos;
    float distance;
    float speed = 30.0f;
    int moveTime = 0;
    int stayTime = 70;
    float moveStartTime = 0.0f;

    bool moveEnd = false;

    [SerializeField] Sprite[] alertIcon;
    [SerializeField] Image iconImage;
    
    // Start is called before the first frame update
    void Start()
    {
        initPos = this.transform.localPosition;
        movePos = this.transform.localPosition;
        movePos.y = 0f;
        distance = Vector3.Distance(initPos, movePos);
    }

    // Update is called once per frame
    void Update()
    {
        if (systemText.text != null) Moveing();
    }

    //システムテキストを表示する
    public void ShowSystemText(string inputText)
    {
        iconImage.sprite = alertIcon[0];
        moveEnd = false;
        moveTime = 0;
        stayTime = 70;
        systemText.text = inputText;
        moveStartTime = Time.time;
    }

    //システムテキストを表示する
    public void ShowSystemWarning(string inputText)
    {
        iconImage.sprite = alertIcon[1];
        moveEnd = false;
        moveTime = 0;
        stayTime = 140;
        systemText.text = inputText;
        moveStartTime = Time.time;
    }

    private void Moveing()
    {
        float presentLocation = 0.0f;

        if (moveEnd == false)
        {
            presentLocation = ((Time.time - moveStartTime) * speed) / distance;
            transform.localPosition = Vector3.Lerp(initPos, movePos, presentLocation);   //画面の上から下に降りてくる。
            if (transform.localPosition == movePos){ moveEnd = true; }                  //目的地に着いたらmoveEndがtrue
        }

        if (moveEnd == true && moveTime >= stayTime)
        {
            presentLocation = ((Time.time - moveStartTime) * speed) / distance;
            transform.localPosition = Vector3.Lerp(movePos, initPos, presentLocation);   //画面の上に帰っていく
            if(transform.localPosition == initPos) { systemText.text = null; }
        }
        if (moveEnd == true && moveTime < stayTime) {
            moveTime++;
            moveStartTime = Time.time;
        }
    }
}
