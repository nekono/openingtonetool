﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineColorController : MonoBehaviour
{
    //拍子の線(グリッド線)の色を変えるUIコントローラー

    [SerializeField] Slider redSlider;
    [SerializeField] Slider greenSlider;
    [SerializeField] Slider blueSlider;
    [SerializeField] Slider alphaSlider;

    [SerializeField] Text redText;
    [SerializeField] Text greenText;
    [SerializeField] Text blueText;
    [SerializeField] Text alphaText;

    [SerializeField] Material material;

    Mesh mesh;

    private void Start()
    {
        
        mesh = new Mesh();
        mesh.vertices = new Vector3[]
        {
            new Vector3(-1f,-1f,0),   //左下
            new Vector3(-1f, 1f,0),   //左上
            new Vector3( 1f,-1f,0),   //右下
            new Vector3( 1f, 1f,0),   //右上
        };

        mesh.uv = new Vector2[]
        {
            new Vector2(0,0),
            new Vector2(0,1),
            new Vector2(1,0),
            new Vector2(1,1),
        };

        mesh.triangles = new int[]
        {
            0,1,2,
            1,3,2,
        };
        
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

    // Update is called once per frame
    void Update()
    {
        redText.text = string.Format("R:{0:000}", redSlider.value * 255);
        greenText.text = string.Format("G:{0:000}", greenSlider.value * 255);
        blueText.text = string.Format("B:{0:000}", blueSlider.value * 255);
        alphaText.text = string.Format("A:{0:000}", alphaSlider.value * 255);

        Vector3 pos = transform.position;
        pos.x += 7f;
        pos.y -= 2f;
        pos.z = 0f;

        Color newColor = new Color(redSlider.value, greenSlider.value, blueSlider.value, alphaSlider.value);
        material.color = newColor;
        Graphics.DrawMesh(mesh, pos, Quaternion.identity, material, 0);
    }
}
