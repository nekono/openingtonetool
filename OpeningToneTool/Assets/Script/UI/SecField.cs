﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecField : MonoBehaviour
{
    [SerializeField]
    SoundManager soundManager;

    [SerializeField]
    UITimeManager uITimeManager;

    [SerializeField]
    InputField secField;

    //InputFieldeからの時間の入力
    public void InputTime()
    {
        int sec = int.Parse(secField.text);
        secField.text = string.Format("{0:00}", sec);
        uITimeManager.TimeProsessing();             //UITimeManagerに時間が変化したことを伝える
    }


    //上ボタンを押されたら時間を追加
    public void IncrementTime()
    {
        int sec = int.Parse(secField.text);
        sec = sec + 1;
        uITimeManager.allFieldTime += 1;

        uITimeManager.TimeProsessing();
    }

    //下ボタンを押されたら時間を追加
    public void DecrementTime()
    {
        int sec = int.Parse(secField.text);
        sec = sec - 1;
        uITimeManager.allFieldTime -= 1;

        uITimeManager.TimeProsessing();
    }
}
