﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicNotesInfoManager : MonoBehaviour
{
    [SerializeField] InputField musicTitle;
    [SerializeField] InputField comporser;
    [SerializeField] InputField notesDesigner;
    [SerializeField] InputField difficult;

    //InputFieldを入力可能にする
    public void isLoadedMusic()
    {
        //musicTitle.interactable = true;
        comporser.interactable = true;
        notesDesigner.interactable = true;
        difficult.interactable = true;
        difficult.text = "1";


        musicTitle.text = GameDataManager.gameDataManagerInstance.musicName;
        NotesDataManager.notesData[0] = musicTitle.text + ",,,,";       //音楽のタイトル

        notesDesigner.text = GameDataManager.gameDataManagerInstance.notesEditor;
        NotesDataManager.notesData[1] = notesDesigner.text + ",,,,";    //譜面作者

        comporser.text = GameDataManager.gameDataManagerInstance.comporser;
        NotesDataManager.notesData[2] = comporser.text + ",,,,";        //作曲者名

        difficult.text = GameDataManager.gameDataManagerInstance.difficult.ToString();
        NotesDataManager.notesData[3] = difficult.text + ",,,,";      //難易度
    }

    //曲名の変更(ファイル名ではなくノーツデータ内のみ)
    public void changeMusicTitle()
    {
        NotesDataManager.notesData[0] = musicTitle.text + ",,,,";      //音楽のタイトル
        GameDataManager.gameDataManagerInstance.musicName = musicTitle.text;
    }

    //ノーツデザイナー名の変更
    public void changeNotesDesignerName()
    {
        NotesDataManager.notesData[1] = notesDesigner.text + ",,,,";    //譜面作者
        GameDataManager.gameDataManagerInstance.notesEditor = notesDesigner.text;
    }

    //作曲者名の変更
    public void changeComporserName()
    {
        NotesDataManager.notesData[2] = comporser.text + ",,,,";        //作曲者名
        GameDataManager.gameDataManagerInstance.comporser = comporser.text;
    }

    //難易度表記の変更
    public void changeDifficult()
    {
        NotesDataManager.notesDifficult = int.Parse(difficult.text);               //難易度
        GameDataManager.gameDataManagerInstance.difficult = int.Parse(difficult.text);
    }
}
