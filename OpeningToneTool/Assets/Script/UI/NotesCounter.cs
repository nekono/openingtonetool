﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotesCounter : MonoBehaviour
{
    //配置されているノーツを数える
    int allCount = 0;
    int normalCount = 0;
    int ngCount = 0;
    int longCount = 0;
    int beforeNotesCount = 0;
    int beforeBPMUnitsCount = 0;

    public Toggle notesInfoToggle;
    public GameObject notesInfoWindow;
    public Text allText;
    public Text normalText;
    public Text ngText;
    public Text longText;

    //ノーツの数を表示するUIを表示
    public void NotesInfoWindow()
    {
        notesInfoWindow.SetActive(notesInfoToggle.isOn);
    }

    private void Update()
    {
        //ノーツの配置や削除の動きがあった場合、カウントする
        if(NotesDataManager.notesCountUpdate || beforeBPMUnitsCount != BPMUnitManager.bpmTimeList.Count)
        {
            NotesCount();
        }
        beforeNotesCount = NotesDataManager.editNotesData.Count;
        beforeBPMUnitsCount = BPMUnitManager.bpmTimeList.Count;
    }

    //ノーツの数を返す
    public int GetAllCount()
    {
        return allCount;
    }

    //ノーツをカウントする
    public void NotesCount()
    {
        allCount = 0;
        normalCount = 0;
        ngCount = 0;
        longCount = 0;

        foreach (NotesData x in NotesDataManager.editNotesData)
        {
            //x.** = (Time,Lane,Kind,Length,BPM)
            switch (x.kind)
            {
                case (int)NotesKind.Normal:
                    allCount++;
                    normalCount++;
                    break;

                case (int)NotesKind.NG:
                    allCount++;
                    ngCount++;
                    break;

                case (int)NotesKind.Long:
                    allCount++;
                    int addCount = (int)((x.bpm / 60) * x.length);
                    if (addCount == 0) addCount = 1;
                    longCount += addCount;
                    break;

                default:
                    break;
            }
        }

        allCount = normalCount + ngCount + longCount;
        normalText.text = string.Format(":{0}", normalCount);
        ngText.text = string.Format(":{0}", ngCount);
        longText.text = string.Format(":{0}", longCount);
        allText.text = string.Format(":{0}", allCount);
    }
}
