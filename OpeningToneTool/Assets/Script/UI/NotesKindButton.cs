﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotesKindButton : MonoBehaviour
{
    public int notesKind;

    [SerializeField] GameObject blueUI;
    [SerializeField] GameObject[] buttons;

    [SerializeField] PutNotes putNotes;

    //配置するノーツの種類を選択、変更
    public void ChangeNormal()
    {
        notesKind = (int)NotesKind.Normal;
        putNotes.longEndSetting = false;    //ロングノーツの先端を置いてる最中に選択しているノーツを変えたらキャンセルする
        blueUI.transform.position = buttons[0].transform.position;
    }

    public void ChangeLong()
    {
        notesKind = (int)NotesKind.Long;
        blueUI.transform.position = buttons[1].transform.position;
    }

    public void ChangeNG()
    {
        notesKind = (int)NotesKind.NG;
        putNotes.longEndSetting = false;    //ロングノーツの先端を置いてる最中に選択しているノーツを変えたらキャンセルする
        blueUI.transform.position = buttons[2].transform.position;
    }
}

