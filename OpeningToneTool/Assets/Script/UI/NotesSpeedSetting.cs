﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotesSpeedSetting : MonoBehaviour
{
    [SerializeField]
    Toggle toggle;

    [SerializeField]
    Toggle gridSettingToggle;

    [SerializeField]
    public Slider slider;

    [SerializeField]
    InputField inputField;

    //グリッドの表示秒数がノーツスピードかどうか
    public bool isNoteSpeed;

    private void Update()
    {
        isNoteSpeed = toggle.isOn;
        if (isNoteSpeed)
        {
            slider.interactable = true;
            //gridSettingToggle.isOn = false;
        }
        else
        {
            slider.interactable = false;
            //gridSettingToggle.isOn = true;
        }
            inputField.text = string.Format("Speed : {0:F1}", slider.value);
    }
}
