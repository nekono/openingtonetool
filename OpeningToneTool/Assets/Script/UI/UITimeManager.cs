﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITimeManager : MonoBehaviour
{

    [SerializeField]
    InputField hour;
    [SerializeField]
    InputField min;
    [SerializeField]
    InputField sec;
    [SerializeField]
    InputField msec;

    [SerializeField]
    SoundManager soundManager;


    public float allFieldTime;  //すべての時間のInputFieldがアクセスして編集する時間


    public void ChangeUITextTime()
    {
        float time = soundManager.playTime;
        hour.text = string.Format("{0:00}", (int)time / 3600);
        min.text = string.Format("{0:00}", (((int)time % 3600) / 60));
        sec.text = string.Format("{0:00}", (int)time % 60);
        msec.text = string.Format("{0:000}", time % 1 * 1000);
        allFieldTime = time;
    }


    private void FixedUpdate()
    {
        ChangeUITextTime();
    }

    //曲の時間を変更するために各時間のInputFieldから時間を取得し、計算した後SoundManagerに投げる
    public void TimeProsessing()
    {
        soundManager.UpdateMusicTime(allFieldTime);
    }
}
