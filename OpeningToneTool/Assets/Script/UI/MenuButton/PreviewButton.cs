﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SFB;
using System.IO;
using System;
using System.Text;
using Game.Data;
using Game.System;

public class PreviewButton : MonoBehaviour
{
    [SerializeField]
    SystemText systemText;

    [SerializeField]
    SoundManager soundManager;

    [SerializeField]
    NotesCounter notesCounter;

    public void OnClick()
    {
        if (notesCounter.GetAllCount() > 0)
        {
            StartPreview();
        }
        else
        {
            systemText.ShowSystemText("1つ以上のノーツを設置してください");
        }
        //PreviewEditData();
    }

    private void StartPreview() {
        GameData.CurrentMusic = soundManager.source.clip;
        GameData.EditData = (NotesDataManager.editNotesData,BPMUnitManager.bpmTimeList);
        GameData.Bpm = BPMUnitManager.bpmTimeList[0].bpm;
        SceneManager.SceneMove(Scenes.Preview).Forget();
    }

    public void PreviewEditData()
    {
        //BPMUnitManager  bpmTimeList 編集用のBPMのみのデータ
        //NotesDataManager  editNotesData  編集用の一時的なノーツのみのデータ(Time,Lane,Kind,Length,BPM)
        //上記二つを保存する(ゲームで使える形式ではない編集用のデータ)

        //保存形式に変換するためのstring
        List<string> previewEditData = new List<string>();


        //セーブする曲の曲名
        string previewMusicTitle;

        //プレビュー用のCSV形式のテキスト入れ用
        string exportCSVData = "";

        //ゲームデータマネージャのデータを保存する
        //previewEditData.Add(string.Format("{0}", GameDataManager.gameDataManagerInstance.musicName));
        previewEditData.Add(string.Format("{0}", GameDataManager.gameDataManagerInstance.difficult));
        previewEditData.Add(string.Format("{0}", GameDataManager.gameDataManagerInstance.notesEditor));
        previewEditData.Add(string.Format("{0}", GameDataManager.gameDataManagerInstance.comporser));


        //BPMのリストに沿って時間内のノーツを保存する
        for (int countBPMUnit = 0; countBPMUnit < BPMUnitManager.bpmTimeList.Count; countBPMUnit++)
        {
            previewEditData.Add(string.Format("BPM,{0}", BPMUnitManager.bpmTimeList[countBPMUnit].bpm));

            //BPMUnitがまだ後ろにある場合にその時間までのノーツを書き出していく
            if (BPMUnitManager.bpmTimeList.Count - 1 > countBPMUnit)
            {
                //ノーツデータを保存する("TypeNotes,Time,Lane,Kind,Length,BPM")
                foreach (var notesData in NotesDataManager.editNotesData)
                {
                    //現在の書き出し対象のBPMUnitの時間以降から次のBPMUnitまでの時間のノーツを書き出す
                    if (notesData.time >= BPMUnitManager.bpmTimeList[countBPMUnit].bpmTime && notesData.time < BPMUnitManager.bpmTimeList[countBPMUnit + 1].bpmTime)
                    {
                        previewEditData.Add(string.Format("{0},{1},{2},{3}", notesData.time, notesData.lane, notesData.kind, notesData.length));
                    }
                }
            }
            //最後のBPMUnitになった場合(もしくはBPM変化がない)
            if (BPMUnitManager.bpmTimeList.Count - 1 == countBPMUnit)
            {
                //ノーツデータを保存する("TypeNotes,Time,Lane,Kind,Length,BPM")
                foreach (var notesData in NotesDataManager.editNotesData)
                {
                    //現在の書き出し対象のBPMUnitの時間以降から曲の最後の時間まで書き出す
                    if (notesData.time >= BPMUnitManager.bpmTimeList[countBPMUnit].bpmTime && notesData.time <= soundManager.MusicLength())
                    {
                        previewEditData.Add(string.Format("{0},{1},{2},{3}", notesData.time, notesData.lane, notesData.kind, notesData.length));
                    }
                }
            }
        }


        foreach (var data in previewEditData) {
            exportCSVData = exportCSVData + data + "\n";
        }

        systemText.ShowSystemText(string.Format("譜面データをプレビュー用に書き出しました"));

    }
}
