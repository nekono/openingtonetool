﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuBar : MonoBehaviour
{
    [SerializeField] Button saveButton;
    [SerializeField] Button exportButton;
    [SerializeField] Button previewButton;


    //セーブとエクスポートを有効にする
    public void isReadySaveExport()
    {
        saveButton.interactable = true;
        exportButton.interactable = true;
        previewButton.interactable = true;
    }
    
}
