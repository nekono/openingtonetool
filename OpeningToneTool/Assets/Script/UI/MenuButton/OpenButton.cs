﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SFB;
using System.IO;

public class OpenButton : MonoBehaviour
{
    [SerializeField]
    SoundManager soundManagerScript;
    [SerializeField]
    UIUpdater uIUpdater;

    [SerializeField]
    GameObject firstBPMUnit;    //曲の始まり用の最初のBPMUnit
    [SerializeField]
    GameObject bpmUnitsCanvas;

    [SerializeField] SystemText systemText;

    [SerializeField] BPMInputter bpmInputter;

    [SerializeField]
    SaveConfirmatior saveConfirmatior;


    public void OnClick()
    {
        var extensions = new[]
        {
            new ExtensionFilter("OPEN OpeningToneToolData","optn")
        };

        var path = StandaloneFileBrowser.OpenFilePanel("Open Music File", Application.dataPath + "/OpeningToneToolNotes/", extensions, true);

        string file = FileManager.GetFilePath(path);
        string fileName = Path.GetFileNameWithoutExtension(file);

        string readFilePath = FileManager.ReadPath(path);                                           //選択した読み込むファイルのパス

        string folderPath = "/OpeningToneToolNotes/" + fileName + "/";      //フォルダの相対パス"/***/music/"
#if UNITY_EDITOR
        string absoluteFolderPath = Directory.GetCurrentDirectory() + folderPath;       //フォルダのパス"/***/music/"
        string absoluteFilePath = absoluteFolderPath + file;  //ファイルのコピー保存先のパス"C:/***/music/music.wav"
#else
        string absoluteFolderPath = System.AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + "/OpeningToneToolNotes/" + fileName + "/";       //フォルダのパス"C:/***/music/"
        string absoluteFilePath = folderPath + file;  //ファイルのコピー保存先のパス"C:/***/music/music.wav"
#endif

        //ファイルの読み込みを行っていない場合はすべてスキップ
        if (readFilePath != null)
        {
            if (soundManagerScript.source)
            {
                soundManagerScript.source.time = 0;
                soundManagerScript.playTime = 0;
            }

            //ノーツデータを初期化する
            NotesDataManager.NotesDataInit(GameDataManager.gameDataManagerInstance.musicName);

            //BPMUnitデータを削除する
            BPMUnitManager.bpmTimeList.Clear();

            //すべてのBPMUnitを削除
            foreach (Transform obj in bpmUnitsCanvas.transform)
            {
                Destroy(obj.gameObject);
            }

            //テキストデータからノーツやBPMのデータを読み込み、生成
            StreamReader streamReader = new StreamReader(@readFilePath);
            GameObject newUnit;

            while (!streamReader.EndOfStream)
            {
                string readLine = streamReader.ReadLine();
                string[] datas = readLine.Split(',');

                if (datas[0] == "TypePath") { GameDataManager.gameDataManagerInstance.path = datas[1]; }
                if (datas[0] == "TypeMusicPath") { GameDataManager.gameDataManagerInstance.musicPath = datas[1]; }
                if (datas[0] == "TypeDifficult") { GameDataManager.gameDataManagerInstance.difficult = int.Parse(datas[1]); }
                if (datas[0] == "TypeMusicName") { GameDataManager.gameDataManagerInstance.musicName = datas[1]; }
                if (datas[0] == "TypeNotesEditor") { GameDataManager.gameDataManagerInstance.notesEditor = datas[1]; }
                if (datas[0] == "TypeComporser") { GameDataManager.gameDataManagerInstance.comporser = datas[1]; }

                if (datas[0] == "TypeBPM") {
                    //曲の最初のBPMUnit
                    if (float.Parse(datas[1]) == 0.0f) {
                        newUnit = Instantiate(firstBPMUnit);
                        BPMUnit newBPMUnit = newUnit.GetComponent<BPMUnit>();
                        if (datas.Length == 3)
                        {
                            newBPMUnit.OpenFirstUnit(datas[2], datas[1], "false");     //曲の最初のUnit限定の処理をする
                        }
                        if (datas.Length == 4)
                        {
                            newBPMUnit.OpenFirstUnit(datas[2], datas[1], datas[3]);     //曲の最初のUnit限定の処理をする
                        }
                        //親オブジェクトをcanvasにする
                        newUnit.transform.localPosition = new Vector2(0f, 0f);
                        newUnit.transform.SetParent(bpmUnitsCanvas.transform, false);
                    }
                    else {
                        Debug.Log(datas.Length + " : " + bool.Parse(datas[3]));
                        if (datas.Length == 3)
                        {
                            bpmInputter.OpenButtonBPMUnitInit(float.Parse(datas[2]), float.Parse(datas[1]), false);
                        }
                        if (datas.Length == 4)
                        {
                            
                            bpmInputter.OpenButtonBPMUnitInit(float.Parse(datas[2]), float.Parse(datas[1]), bool.Parse(datas[3]));
                        }
                    }
                }

                if(datas[0] == "TypeNotes")
                {
                    NotesData putData = new NotesData();
                    putData.time = float.Parse(datas[1]);
                    putData.lane = int.Parse(datas[2]);
                    putData.kind = int.Parse(datas[3]);
                    putData.length = float.Parse(datas[4]);
                    putData.bpm = float.Parse(datas[5]);
                    if (NotesDataManager.isCanPutNotes(putData)) { NotesDataManager.editNotesData.Add(putData); }
                }

            }

            //サウンドマネージャーに曲のパスを送る
            soundManagerScript.ReceiveMusic(Application.dataPath + GameDataManager.gameDataManagerInstance.musicPath);

            uIUpdater.MusicTitleUpdate();

            saveConfirmatior.isMusicLoaded = true;

            systemText.ShowSystemText("編集中の譜面のデータと曲を取り込みました");
        }

    }



}
