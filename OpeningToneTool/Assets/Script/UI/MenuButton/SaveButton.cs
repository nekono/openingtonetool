﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SFB;
using System.IO;
using System;
using System.Text;

public class SaveButton : MonoBehaviour
{
    [SerializeField]
    SystemText systemText;

    [SerializeField]
    SaveConfirmatior saveConfirmatior;

    public void OnClick()
    {
        SaveEditData();
    }



    public void SaveEditData()
    {
        //BPMUnitManager  bpmTimeList 編集用のBPMのみのデータ
        //NotesDataManager  editNotesData  編集用の一時的なノーツのみのデータ(Time,Lane,Kind,Length,BPM)
        //上記二つを保存する(ゲームで使える形式ではない編集用のデータ)

        //保存形式に変換するためのstring
        List<string> saveEditData = new List<string>();

        //セーブ先のフォルダーのパス
        string saveFolderPath;

        //セーブする曲の曲名
        string saveMusicTitle;

        //セーブするファイル名("MusicTitle_難易度")
        string saveFileTitle;

        //セーブ処理実行後の返り値が入るパス
        string saveReturnPath;

        //ゲームデータマネージャのデータを保存する
        saveEditData.Add(string.Format("TypePath,{0}", GameDataManager.gameDataManagerInstance.path));
        saveEditData.Add(string.Format("TypeMusicPath,{0}", GameDataManager.gameDataManagerInstance.musicPath));
        saveEditData.Add(string.Format("TypeDifficult,{0}", GameDataManager.gameDataManagerInstance.difficult));
        saveEditData.Add(string.Format("TypeMusicName,{0}", GameDataManager.gameDataManagerInstance.musicName));
        saveEditData.Add(string.Format("TypeNotesEditor,{0}", GameDataManager.gameDataManagerInstance.notesEditor));
        saveEditData.Add(string.Format("TypeComporser,{0}", GameDataManager.gameDataManagerInstance.comporser));


        //BPMを保存する("TypeBPM, BPMTime, BPM, BPMKind")
        foreach (var bpmData in BPMUnitManager.bpmTimeList){

            Debug.Log(string.Format("TypeBPM,{0},{1},{2}", bpmData.bpmTime, bpmData.bpm, bpmData.bpmKind));
            saveEditData.Add(string.Format("TypeBPM,{0},{1},{2}", bpmData.bpmTime, bpmData.bpm, bpmData.bpmKind));
        }

        //ノーツデータを保存する("TypeNotes,Time,Lane,Kind,Length,BPM")
        foreach (var notesData in NotesDataManager.editNotesData) { saveEditData.Add(string.Format("TypeNotes,{0},{1},{2},{3},{4}", notesData.time, notesData.lane, notesData.kind, notesData.length, notesData.bpm)); }

        saveFolderPath = Application.dataPath + GameDataManager.gameDataManagerInstance.path;     //セーブするフォルダーのパス
        saveMusicTitle = GameDataManager.gameDataManagerInstance.musicName; //セーブする曲名
        saveFileTitle = string.Format("{0}_{1}", saveMusicTitle, GameDataManager.gameDataManagerInstance.difficult);

        //ファイルブラウザーを開き、保存するフォルダー先のパスとファイル名まで入れたstringを取得
        saveReturnPath = StandaloneFileBrowser.SaveFilePanel("Save File", saveFolderPath, saveFileTitle, "optn");


        using (var streamWriter = new StreamWriter(@saveReturnPath, false, Encoding.GetEncoding("utf-8")))
        {
            foreach (var data in saveEditData){ streamWriter.Write(data + "\n"); }            
        }

        systemText.ShowSystemText("編集データをセーブしました");

        saveConfirmatior.isNotSaved = false;
    }
}
