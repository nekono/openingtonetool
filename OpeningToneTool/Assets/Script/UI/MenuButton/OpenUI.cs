﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenUI : MonoBehaviour
{
    [SerializeField]
    Button OpenButton;

    [SerializeField]
    Button returnHome;

    [SerializeField]
    GameObject me;

    public void openUI()
    {
        me.SetActive(true);
    }

    public void closeUI()
    {
        me.SetActive(false);
    }
}
