﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SFB;
using System.IO;


public class NewButton : MonoBehaviour
{
    [SerializeField]
    SoundManager soundManagerScript;
    [SerializeField]
    UIUpdater uIUpdater;

    [SerializeField]
    GameObject firstBPMUnit;    //曲の始まり用の最初のBPMUnit
    [SerializeField]
    GameObject bpmUnitsCanvas;

    [SerializeField] SystemText systemText;

    [SerializeField]
    SaveConfirmatior saveConfirmatior;


    public void OnClick()
    {
        var extensions = new[]
        {
            new ExtensionFilter("wav","wav")
        };

        var path = StandaloneFileBrowser.OpenFilePanel("Open Music File", "", extensions, true);

        string file = FileManager.GetFilePath(path);
        string fileName = Path.GetFileNameWithoutExtension(file);

        string readFilePath = FileManager.ReadPath(path);                   //選択した読み込むファイルのパス

        string folderPath = "/OpeningToneToolNotes/" + fileName + "/";      //フォルダの相対パス"/***/music/"
        string filePath = folderPath + file;                                //ファイルのコピー保存先の相対パス"/***/music/music.wav"
        string absoluteFolderPath = Application.dataPath + folderPath;      //フォルダの絶対パス
        string absoluteFilePath = Application.dataPath + filePath;          //ファイルの絶対パス

        //ファイルの読み込みを行っていない場合はすべてスキップ
        if (readFilePath != null)
        {
            //フォルダの存在を確認
            if (!Directory.Exists(folderPath))
            {
                DirectoryInfo di = new DirectoryInfo(absoluteFolderPath);   //フォルダ作成
                di.Create();
            }

            //曲の存在を確認
            if (!File.Exists(absoluteFilePath))
            {
                File.Copy(readFilePath, absoluteFilePath);                  //作業フォルダに曲のコピーを作成
            }


            //マネージャーにフォルダのパスと曲名を送る
            GameDataManager.gameDataManagerInstance.path = folderPath;
            GameDataManager.gameDataManagerInstance.musicPath = filePath;
            GameDataManager.gameDataManagerInstance.musicName = fileName;

            uIUpdater.MusicTitleUpdate();

            //サウンドマネージャーに曲のパスを送る
            soundManagerScript.ReceiveMusic(absoluteFilePath);

            //ノーツデータを初期化する
            NotesDataManager.NotesDataInit(fileName);
            //BPMUnitデータを削除する
            BPMUnitManager.bpmTimeList.Clear();


            //すべてのBPMUnitを削除
            foreach(Transform obj in bpmUnitsCanvas.transform)
            {
                Destroy(obj.gameObject);
            }

            //曲の始まり用のBPMUnitの生成
            GameObject newUnit = Instantiate(firstBPMUnit);
            BPMUnit newBPMUnit = newUnit.GetComponent<BPMUnit>();
            newBPMUnit.FirstUnit();     //曲の最初のUnit限定の処理をする
            //親オブジェクトをcanvasにする
            newUnit.transform.localPosition = new Vector2(0f, 0f);
            newUnit.transform.SetParent(bpmUnitsCanvas.transform, false);

            saveConfirmatior.isMusicLoaded = true;

            systemText.ShowSystemText("新たな譜面データと曲のデータを取り込み、作成しました");
        }
    }
}
