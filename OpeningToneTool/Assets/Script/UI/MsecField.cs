﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MsecField : MonoBehaviour
{
    [SerializeField]
    SoundManager soundManager;

    [SerializeField]
    UITimeManager uITimeManager;

    [SerializeField]
    InputField msecField;

    //InputFieldeからの時間の入力
    public void InputTime()
    {
        float msec = int.Parse(msecField.text);
        msecField.text = string.Format("{0:000}", msec);
        uITimeManager.TimeProsessing();             //UITimeManagerに時間が変化したことを伝える
    }


    //上ボタンを押されたら時間を追加
    public void IncrementTime()
    {
        float msec = int.Parse(msecField.text) / 100;
        msec = msec + 0.01f;
        uITimeManager.allFieldTime += 0.001f;

        uITimeManager.TimeProsessing();
    }

    //下ボタンを押されたら時間を追加
    public void DecrementTime()
    {
        float msec = int.Parse(msecField.text) / 100;
        msec = msec - 0.01f;
        uITimeManager.allFieldTime -= 0.001f;

        uITimeManager.TimeProsessing();
    }
}
