﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HourField : MonoBehaviour
{
    [SerializeField]
    SoundManager soundManager;

    [SerializeField]
    UITimeManager uITimeManager;

    [SerializeField]
    InputField hourField;

    //InputFieldeからの時間の入力
    public void InputTime()
    {
        int hour = int.Parse(hourField.text);
        hourField.text = string.Format("{0:00}", hour);
        uITimeManager.TimeProsessing();             //UITimeManagerに時間が変化したことを伝える
    }


    //上ボタンを押されたら時間を追加
    public void IncrementTime()
    {
        int hour = int.Parse(hourField.text);
        hour = hour + 1;
        uITimeManager.allFieldTime += 3600;

        uITimeManager.TimeProsessing();
    }

    //下ボタンを押されたら時間を追加
    public void DecrementTime()
    {
        int hour = int.Parse(hourField.text);
        hour = hour - 1;
        uITimeManager.allFieldTime -= 3600;

        uITimeManager.TimeProsessing();
    }
}
