﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridSetting : MonoBehaviour
{
    [SerializeField]
    Toggle toggle;

    [SerializeField]
    Toggle speedToggle;

    [SerializeField]
    public Slider slider;

    [SerializeField]
    InputField inputField;

    //グリッドの表示秒数がフレーム固定かどうか
    public bool isGridFrame;

    private void Update()
    {
        isGridFrame = toggle.isOn;
        if (isGridFrame)
        {
            slider.interactable = true;
            //speedToggle.isOn = false;
        }
        else
        {
            slider.interactable = false;
            //speedToggle.isOn = true;
        }

        inputField.text = string.Format("Frame :{0:F2}s", slider.value);
    }

}
