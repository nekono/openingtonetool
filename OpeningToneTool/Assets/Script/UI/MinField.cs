﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinField : MonoBehaviour
{
    [SerializeField]
    SoundManager soundManager;

    [SerializeField]
    UITimeManager uITimeManager;

    [SerializeField]
    InputField minField;

    //InputFieldeからの時間の入力
    public void InputTime()
    {
        int min = int.Parse(minField.text);
        minField.text = string.Format("{0:00}", min);
        uITimeManager.TimeProsessing();             //UITimeManagerに時間が変化したことを伝える
    }


    //上ボタンを押されたら時間を追加
    public void IncrementTime()
    {
        int min = int.Parse(minField.text);
        min = min + 1;
        uITimeManager.allFieldTime += 60;

        uITimeManager.TimeProsessing();
    }

    //下ボタンを押されたら時間を追加
    public void DecrementTime()
    {
        int min = int.Parse(minField.text);
        min = min - 1;
        uITimeManager.allFieldTime -= 60;

        uITimeManager.TimeProsessing();
    }
}
