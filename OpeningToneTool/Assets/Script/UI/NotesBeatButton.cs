﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotesBeatButton : MonoBehaviour
{
    //選択しているノーツを置く間隔
    public float beat = 1;

    [SerializeField]
    Image beat4;

    [SerializeField]
    Image beat8;

    [SerializeField]
    Image beat16;

    [SerializeField]
    Image beat32;

    [SerializeField]
    Sprite uiSprite;

    [SerializeField]
    Sprite background;

    [SerializeField]
    BeatLine bpmLineDrawer;

    private void Start()
    {
        beat4.sprite = background;
    }


    public void Beat4()
    {
        beat = 1f;
        beat4.sprite = background;
        beat8.sprite = uiSprite;
        beat16.sprite = uiSprite;
        beat32.sprite = uiSprite;
        bpmLineDrawer.choiceBeat = beat;
    }

    public void Beat8()
    {
        beat = 2f;
        beat4.sprite = uiSprite;
        beat8.sprite = background;
        beat16.sprite = uiSprite;
        beat32.sprite = uiSprite;
        bpmLineDrawer.choiceBeat = beat;
    }

    public void Beat16()
    {
        beat = 4f;
        beat4.sprite = uiSprite;
        beat8.sprite = uiSprite;
        beat16.sprite = background;
        beat32.sprite = uiSprite;
        bpmLineDrawer.choiceBeat = beat;
    }

    public void Beat32()
    {
        beat = 8f;
        beat4.sprite = uiSprite;
        beat8.sprite = uiSprite;
        beat16.sprite = uiSprite;
        beat32.sprite = background;
        bpmLineDrawer.choiceBeat = beat;
    }
}
