﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIUpdater : MonoBehaviour
{
    [SerializeField]
    public InputField musicTitle;


    //曲名の表示更新
    public void MusicTitleUpdate()
    {
        musicTitle.text = GameDataManager.gameDataManagerInstance.musicName;
    }


}
