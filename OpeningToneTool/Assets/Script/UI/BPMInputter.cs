﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BPMInputter : MonoBehaviour
{
    [SerializeField]
    InputField bpmValueInputField;
    [SerializeField]
    SoundManager soundManager;
    [SerializeField]
    GameObject bpmUnit;             //譜面のBPM
    [SerializeField]
    GameObject musicBpmUnit;        //曲のBPM
    [SerializeField]
    Transform bpmUnitsCanvas;



    //BPMUnitを生成する(譜面のBPM変化)
    public void BPMUnitInstantiater()
    {
        BPMUnitChecker(bpmUnit, false);
    }

    //MusicBPMUnitを生成する(曲自体のBPM変化)
    public void MusicBPMUnitInstantiater()
    {
        BPMUnitChecker(musicBpmUnit, true);
    }

    //BPMUnitを挿入する際に整合性をチェックする
    public void BPMUnitChecker(GameObject unit, bool bpmKind)
    {
        //BPMが入力されているかを確認
        if (bpmValueInputField.text.Length != 0)
        {
            //BPMが負数や0ではないかを確認
            if (float.Parse(bpmValueInputField.text) > 0.0f)
            {
                //既に同じ時間にBPMUnitが存在していないかを確認
                if (!BPMUnitManager.SearchListBPMTime(soundManager.playTime))
                {
                    //インスタんてぃえーとする
                    GameObject newUnit = Instantiate(unit);
                    //親オブジェクトをcanvasにする
                    newUnit.transform.SetParent(bpmUnitsCanvas.transform, false);

                    //BPMUnitをGetComponentして、BPMをtextにセットする
                    BPMUnit newBPMUnit = newUnit.GetComponent<BPMUnit>();
                    newBPMUnit.SetBPM(float.Parse(bpmValueInputField.text), soundManager.playTime, bpmKind);

                    bpmValueInputField.text = "";
                }
            }
            else
            {
                bpmValueInputField.text = "";
            }
        }
        else
        {
            bpmValueInputField.text = "";
        }
    }


    //Openボタンで読み込み時にBPMUnitを生成
    public void OpenButtonBPMUnitInit(float bpmData, float timeData, bool bpmKind)
    {
        //BPMUnitをInstantiateする
        GameObject newUnit = Instantiate(bpmKind == true ? musicBpmUnit : bpmUnit);

        //親オブジェクトをcanvasにする
        newUnit.transform.SetParent(bpmUnitsCanvas.transform, false);

        //BPMUnitをGetComponentして、BPMをtextにセットする
        BPMUnit newBPMUnit = newUnit.GetComponent<BPMUnit>();
        newBPMUnit.SetBPM(bpmData, timeData, bpmKind);
        bpmValueInputField.text = "";
    }
}
